#-*- coding: utf-8 -*- #
# Copyright (C) 2015 Michał Góral.
#
# This file is part of quicksend
#
# quicksend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# quicksend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with quicksend. If not, see <http://www.gnu.org/licenses/>.

import sys
import subprocess

class lazy_exec:
    def __init__(self, cmd, unsafe = False):
        self._cmd = cmd
        self._use_shell = bool(unsafe)
        self._out = None

    def __str__(self):
        if self._out is not None:
            return self._out

        self._out = subprocess.check_output(
            self._cmd, shell = self._use_shell).decode(sys.stdout.encoding).strip()
        return self._out
