#-*- coding: utf-8 -*- #
# Copyright (C) 2015 Michał Góral.
#
# This file is part of quicksend
#
# quicksend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# quicksend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with quicksend. If not, see <http://www.gnu.org/licenses/>.

import voluptuous
import weakref

from qs.detail.locale import _
from qs.detail.codes import ExceptionWithCode, ErrorCode

class IncorrectConfiguration(ExceptionWithCode):
    def __init__(self, msg):
        super().__init__(ErrorCode.BAD_INPUT, msg)

class _ConfigurationMeta(type):
    def __new__(cls, name, bases, attrs):
        newcls= super().__new__(cls, name, bases, {})

        newcls.__allowed_attrs__ = set()

        for name, obj in attrs.items():
            if isinstance(obj, Property):
                true_name = "_%s_val" % name
                if obj.name is None:
                    obj.name = true_name
                newcls.add_attr(true_name, None)

            newcls.add_attr(name, obj)

        return newcls

    def add_attr(cls, name, val):
        setattr(cls, name, val)
        cls.__allowed_attrs__.add(name)

class Property:
    def __init__(self,
            default = None,
            mutable = True,
            validator = None,
            wrapped_attr = None,
            desc = "",
            metavar = None):
        self.name = wrapped_attr
        self.default = default
        self.mutable = mutable
        self.validator = validator
        self.description = desc
        self.metavar = metavar

    def __get__(self, obj, objtype = None):
        if obj is None:
            return self

        ret = obj[self.name]
        if ret is None and self.default is not None:
            ret = self.default

        return ret

    def __set__(self, obj, value):
        if self.mutable is False:
            raise IncorrectConfiguration(_("%s is immutable") % self.name)

        obj[self.name] = value

class ProtocolConfiguration(metaclass=_ConfigurationMeta):
    __refs__ = []

    def __init__(self, name, protocol, **kwargs):
        self._section_name = name
        self._protocol = protocol

        for kname, value in kwargs.items():
            self[kname] = value

        self.__refs__.append(weakref.ref(self))

    @classmethod
    def instances(cls):
        for ref in cls.__refs__:
            obj = ref()
            if obj is not None:
                yield obj

    def get(self, name, default = None):
        ret = self[name]
        if ret is None:
            return default
        return ret

    def attributes(self):
        return [ a for a in self.__allowed_attrs__ if a[0] != "_" ]

    def validate(self):
        # Dirty, dirty hack. (Maybe not that dirty, but it depends on fact
        # that Property descriptor returns self when __get__'s obj is None)
        cls = self.__class__

        for attr_name in self.attributes():
            validator = getattr(cls, attr_name).validator
            if validator is None:
                continue

            attr = self[attr_name]
            try:
                self[attr_name] = validator(attr)
            except voluptuous.Invalid:
                d = dict(value = attr, field = attr_name, section = self._section_name)
                if attr is None:
                    msg = _("%(section)s: '%(field)s' is not set") % d
                else:
                    msg = _("%(section)s: '%(field)s' can't be set to '%(value)s'") % d
                raise IncorrectConfiguration(msg)

    @property
    def protocol(self):
        return self._protocol

    @property
    def name(self):
        return self._section_name

    def __setattr__(self, name, value):
        if name not in self.__allowed_attrs__ and name[0] != "_":
            raise IncorrectConfiguration(_("'%(name)s' isn't %(protocol)s's attribute") %
                dict(name = name, protocol = self.protocol))

        super().__setattr__(name, value)

    def __getitem__(self, name):
        return getattr(self, name)

    def __setitem__(self, name, value):
        setattr(self, name, value)

