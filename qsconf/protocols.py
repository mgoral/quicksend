#-*- coding: utf-8 -*- #
# Copyright (C) 2015 Michał Góral.
#
# This file is part of quicksend
#
# quicksend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# quicksend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with quicksend. If not, see <http://www.gnu.org/licenses/>.

from functools import wraps

from voluptuous import Schema, All, Any, Required, Boolean, Coerce, truth
from voluptuous import Invalid

from qsconf._protocol_base import ProtocolConfiguration, Property
from qs.detail.locale import _

def _IgnoreCase(expected, msg=None):
    """Validate whether string is equal in case-insensitive manner."""

    @wraps(_IgnoreCase)
    def f(v):
        try:
            v = v.lower()
        except:
            raise Invalid("expected a string")

        if v != expected:
            raise Invalid("string differs")
        return v
    return f

@truth
def _NotNone(value):
    return value is not None

class xmpp(ProtocolConfiguration):
    jid = Property(validator = Schema(All(_NotNone, Coerce(str))),
        metavar = _("STRING"),
        desc = _("user JID (Jabber ID)"))
    password = Property(validator = Schema(Any(None, Coerce(str))),
        metavar = _("STRING"),
        desc = _("password used to login to the server. Avoid this option "
            "at all costs, especially on multi-user systems"))
    host = Property(validator = Schema(Any(None, Coerce(str))),
        metavar = _("ADDRESS"),
        desc = _("SMTP server address"))
    port = Property(validator = Schema(Any(None, Coerce(int))),
        metavar = _("NUMBER"),
        desc = _("SMTP server port number"))
    security = Property(validator = Schema(Any(None, _IgnoreCase("tls"), _IgnoreCase("ssl"))),
        metavar = _("TLS|SSL"),
        desc = _("type of secure SMTP login method, if used"))
    ask_pass = Property(default = True, validator = Schema(Boolean()),
        metavar = _("BOOL"),
        desc = _("ask for password"))
    contacts = Property(validator = Schema(Any(None, {str : str})))

    def __init__(self, name, **kwargs):
        super().__init__(name, "xmpp", **kwargs)

class mail(ProtocolConfiguration):
    sender = Property(validator = Schema(All(_NotNone, Coerce(str))),
        metavar = _("STRING"),
        desc = _("sender's mail address"))
    user = Property(validator = Schema(Any(None, Coerce(str))),
        metavar = _("STRING"),
        desc = _("username used to login to the server"))
    password = Property(validator = Schema(Any(None, Coerce(str))),
        metavar = _("STRING"),
        desc = _("password used to login to the server. Avoid this option "
            "at all costs, especially on multi-user systems"))
    host = Property(validator = Schema(All(_NotNone, Coerce(str))),
        metavar = _("ADDRESS"),
        desc = _("SMTP server address"))
    port = Property(default = 25, validator = Schema(Coerce(int)),
        metavar = _("NUMBER"),
        desc = _("SMTP server port number"))
    security = Property(validator = Schema(Any(None, _IgnoreCase("starttls"), _IgnoreCase("ssl"))),
        metavar = _("STARTTLS|SSL"),
        desc = _("type of secure SMTP login method, if used"))
    ask_pass = Property(default = True, validator = Schema(Boolean()),
        metavar = _("BOOL"),
        desc = _("ask for password"))
    contacts = Property(validator = Schema(Any(None, {str : str})))

    def __init__(self, name, **kwargs):
        super().__init__(name, "mail", **kwargs)
