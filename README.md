qs (quicksend) - send fast, one-shot messages of various formats/protocols

## Installation

Setuptools are great for libraries - not so good for standalone scripts/programs. That's why
quicksend employs the other method of distribution: creating executable zip files. You can simply
invoke the following commands to get it done:

    $ ./configure
    $ make
    $ sudo make install

Or to install in your `$HOME` directory:

    $ ./configure --prefix=~/.local
    $ make
    $ make install

Above commands will install everything, including manpages. If you do not wish to install them, you
should run `configure --no-install-man`. It will force `make install` to skip installing manpages.
You can still install man by running `make install-man`.

Quicksend's dependencies are installed automatically during installation.

## Usage

First you should add configuration of services you want to use to `~/.config/quicksend/qsrc.py`. You
can e.g. modify [qsrc.py.example][qsrc.py.example] to your liking. When they are here it's simply a
matter of executing `qs` application.

For example, to send XMPP messages:

    $ qs xmpp --to Alice 'Hi Alice, did you get my public key from the company directory?'

Or e-mail:

    $ qs mail --to honey@example.com --bcc me  "I love you, Honey Bunny."

You can (and probably should) learn more about these commands by running `qs help`, or even better
`qs help <command-name>`.

## Dependencies

* Python 3
* [virtualenv](https://github.com/pypa/virtualenv)
* [pandoc](http://pandoc.org) *[optional, recommended]* for generation of documentation

## Further documentation

For more in-depth documentation see [docs][docs] directory or type `man qs`.

## License

quicksend is free software: you can redistribute it and/or modify it under the terms of the GNU
General Public License as published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

quicksend is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

For details see [LICENSE][license] file which should be distributed along with quicksend.

  [docs]: docs/
  [license]: LICENSE
  [qsrc.py.example]: docs/howto/qsrc.py.example
