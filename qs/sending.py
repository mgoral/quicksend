#-*- coding: utf-8 -*-
#
# Copyright (C) 2015 Michał Góral.
# 
# This file is part of quicksend
# 
# quicksend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# quicksend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with quicksend. If not, see <http://www.gnu.org/licenses/>.

import abc

from qs.detail.codes import ExceptionWithCode, ErrorCode

class make_sender:
    """Sending wrapper which should be used in client code.
    Usually 'Sender' objects shouldn't be created directly, but via a 'with' statement, which
    ensures that Sender.disconnect() is followed after connecting.

    Example:
    protocol_config = {
        'server' : 'protocol.example.com',
        'server_port' : 321,
        'login': 'Carol',
        'password' : 'secret',
    }
    with make_sender(ProtocolSender, protocol_config) as s:
        s.send(['Alice', 'Bob'], 'Hi, how are you guys doing?')

    """

    def __init__(self, SenderClass, *args, **kwargs):
        self._sender = SenderClass(*args, **kwargs)

    def __enter__(self):
        connected = self._sender.connect()
        if connected is not True:
            raise ExceptionWithCode(ErrorCode.NO_CONNECT)

        return self._sender

    def __exit__(self, type, value, traceback):
        self._sender.disconnect()

class Sender:
    """Sending interface which should be implemented by each concrete sender."""

    __metaclass__ = abc.ABCMeta

    def __init__(self, configuration):
        """Creates an instance of a Sender. 'configuration' is a dictionary containing fields needed
        by a specific protocol implementation for a correct connecting and disconnecting from server
        (like server address, username and password and so on).

        Creating a Sender does not establish a connection. It is usually established by a call to
        Sender.connect(). Some implementations might however establish a one-shot connection during
        message sending. In that case Sender.connect and Sender.disconnect might be left as
        no-ops."""
        self._configuration = configuration

    @property
    def configuration(self):
        return self._configuration

    def connect(self):
        """Can be reimplemented but for one-shot connections (connect-send-disconnect being a single
        message) it might be left as no-op.
        Returns bool indicating whether connection was successfull."""
        return True

    def disconnect(self):
        """Can be reimplemented but for one-shot connections (connect-send-disconnect being a single
        message) it might be left as no-op."""
        pass

    @abc.abstractmethod
    def send(self, msg, *args, **kwargs):
        """Sends a message 'msg'. Some protocols might also use it as one-shot connection
        (connect-send-disconnect being a single message).

        This method is required to be reimplemented."""
        return ErrorCode.NO_ERROR

