#-*- coding: utf-8 -*- #
# Copyright (C) 2016 Michał Góral.
#
# This file is part of quicksend
#
# quicksend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# quicksend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with quicksend. If not, see <http://www.gnu.org/licenses/>.

import argparse
from qs.detail.locale import _

def prepare_options_from_conf(name, protocol):
    parser = argparse.ArgumentParser(prog = name)

    conf_options = parser.add_argument_group(_("configuration options"))
    conf_options.add_argument("-c", "--configuration", metavar = _("NAME"),
        help = _("configuration to be used"))

    cls = protocol.__class__
    attrs = protocol.attributes()
    attrs.sort()

    for attr_name in attrs:
        if attr_name in ["contacts"]:
            continue

        attr = getattr(cls, attr_name)
        conf_options.add_argument("--%s" % attr_name, dest = attr_name, metavar = attr.metavar,
            help = attr.description)

    return parser
