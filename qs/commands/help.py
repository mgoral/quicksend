#-*- coding: utf-8 -*-
#
# Copyright (C) 2015 Michał Góral.
# 
# This file is part of quicksend
# 
# quicksend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# quicksend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with quicksend. If not, see <http://www.gnu.org/licenses/>.

import argparse
import importlib
import logging
import subprocess

from qs.detail.codes import ErrorCode
from qs.detail.locale import _

log = logging.getLogger("quicksend.%s" % __name__)

_parser = argparse.ArgumentParser(prog = "qs help")
_parser.add_argument("help_for", metavar = "command", nargs = "?", help = _("name of a subcommand"))

class Command:
    name = "help"
    description = _("show command help")

    _call_to_global_help = None

    def execute(self, config, env, global_args, command_args):
        args = _parser.parse_args(command_args)

        if args.help_for is None:
            assert self._call_to_global_help is not None, "global parser.print_help wasn't loaded"
            self._call_to_global_help()
            return ErrorCode.NO_ERROR

        command_exists = True
        try:
            py_mod = importlib.import_module("qs.commands.%s" % args.help_for)
        except ImportError:
            command_exists = False

        # The whole poit here is to disallow displaying e.g. "man ls" via qs help ls.
        if command_exists is True:
            man_command = "qs-%s" % args.help_for
        elif args.help_for.startswith("qs"):
            man_command = args.help_for
        else:
            man_command = "qs%s" % args.help_for

        try:
            subprocess.check_call(["man", man_command])
        except subprocess.CalledProcessError as e:
            if command_exists is True:
                py_mod.Command.print_help()
            else:
                return ErrorCode.BAD_INPUT

        return ErrorCode.NO_ERROR

    @classmethod
    def print_help(self):
        _parser.print_help()

    def store_global_help(self, f):
        # A hack done to correctly display the same help for 'qs -h' or 'qs help' calls.
        self._call_to_global_help = f
