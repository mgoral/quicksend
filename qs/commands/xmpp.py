#-*- coding: utf-8 -*-
#
# Copyright (C) 2015 Michał Góral.
# 
# This file is part of quicksend
# 
# quicksend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# quicksend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with quicksend. If not, see <http://www.gnu.org/licenses/>.

import argparse
import logging

from qs.commands._interface import ProtocolInterface
from qs.commands._args import prepare_options_from_conf
import qs.senders.xmpp as xmpp
from qs.sending import  make_sender
from qs.config import get_contacts
from qsconf.protocols import xmpp as xmpp_conf

from qs.detail.codes import ErrorCode, ExceptionWithCode
from qs.detail.locale import _

_parser = prepare_options_from_conf("qs xmpp", xmpp_conf(None))
_message_options = _parser.add_argument_group(_("message options"))
_message_options.add_argument("--to", action = "append", metavar = _("RECIPIENT"), required = True,
    help = _("message recipient. May be specified multiple times"))
_message_options.add_argument("-F", "--from-file", dest = "msg_file", metavar = _("FILE"),
    help = _("send message stored in a file"))
_message_options.add_argument("message", metavar = _("MESSAGE"), nargs = "*", help = _("message body"))

log = logging.getLogger("quicksend.%s" % __name__)

class Command(ProtocolInterface):
    def __init__(self):
        super().__init__(name = "xmpp", description = _("send XMPP (Jabber) message"))

    def execute(self, config, env, global_args, command_remainder):
        args = _parser.parse_args(command_remainder)
        args.message = self._read_message(args)

        configuration = self._get_configuration(config, args, "jid", "jid")
        recipients = get_contacts(configuration, set(args.to))

        if (global_args.verbose is True):
            print("TO: %s" % recipients)
            print(args.message)

        ec = ErrorCode.NO_ERROR
        with make_sender(xmpp.XMPP, configuration, env) as s:
            ec = s.send(args.message, recipients)
        return ec

    @classmethod
    def print_help(self):
        _parser.print_help()

    def _get_message(self, args):
        return " ".join(args.message)
