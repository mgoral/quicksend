#-*- coding: utf-8 -*-
#
# Copyright (C) 2015 Michał Góral.
#
# This file is part of quicksend
#
# quicksend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# quicksend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License

import argparse
import pprint

from qs.detail.codes import ErrorCode, ExceptionWithCode
from qs.detail.locale import _

_parser = argparse.ArgumentParser(prog = "qs show-config")
_parser.add_argument("--with-protocol", dest = "print_protocol", action = "store_true",
    help = _("also print configuration's protocols"))
_parser.add_argument("--with-passwords", dest = "print_pass", action = "store_true",
    help = _("show passwords when for a specified section"))
_parser.add_argument("name", nargs = "?")

class Command:
    name = "show-config"
    description = _("show available configurations")

    def execute(self, config, env, global_args, command_args):
        args = _parser.parse_args(command_args)
        if args.name is not None:
            return self._print_section(config, args)
        else:
            return self._print_section_names(config, args)

    @classmethod
    def print_help(self):
        _parser.print_help()

    def _print_section(self, config, args):
        section = config.get_section(args.name)
        if section is None:
            raise ExceptionWithCode(
                ErrorCode.BAD_INPUT, _("No such section: '%s'" % args.name))
        attrs = section.attributes()
        attrs.sort()

        if args.print_protocol is True:
            print ("protocol: %s" % section.protocol)

        for attr in attrs:
            if args.print_pass is False and "password" in attr.lower():
                print("%s: ****" % attr)
            else:
                val = getattr(section, attr)
                if val is not None:
                    print("%s: %s" % (attr, val))
        return ErrorCode.NO_ERROR

    def _print_section_names(self, config, args):
        sections = config.find_sections(self._section_has_any_protocol)
        sections.sort()

        for s in sections:
            if args.print_protocol is True:
                cr = config.get_section(s)
                print ("%s: %s" % (s, cr.protocol))
            else:
                print(s)
        return ErrorCode.NO_ERROR

    def _section_has_any_protocol(self, section):
        return section.get("protocol") is not None
