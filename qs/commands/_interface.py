#-*- coding: utf-8 -*-
#
# Copyright (C) 2015 Michał Góral.
# 
# This file is part of quicksend
# 
# quicksend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# quicksend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with quicksend. If not, see <http://www.gnu.org/licenses/>.

import sys
import getpass
import voluptuous

from qs.config import choose_section

from qs.detail.codes import ExceptionWithCode, ErrorCode
from qs.detail.locale import _

class CommandInterface:
    """Interface for commands. Commands doesn't necessarily have to inherit this class but they
    should provide each field and method specified here."""

    name = None
    description = None

    def execute(self, config, env, global_args, command_remainder):
        """Execute command according to the options given by a user. Return status of command
        execution.
        @global_args: global arguments parsed by argparse (in argparse.Namespace).
        @command_remainder: list of remaining arguments, which are passed to the command unmodified.
        """
        return ErrorCode.NO_ERROR

    @classmethod
    def print_help(self):
        """Prints help for a given command. Note that this is a classmethod. If Command
        implementation uses argparse it means that ArgumentParser will be probably a global in scope
        of a module."""
        pass

class ProtocolInterface(CommandInterface):
    """Provides an interface with some handy methods for all commands used for sending messages."""

    def __init__(self, name, description):
        self.name = name
        self.description = description
        self._cr = None

    def _get_configuration(self, config, args, sender_field, user_field = None):
        """Returns a dictionary containing user configuration read from the config file and
        overriden with command-line options. If 'user_field' is given, configuration will be checked
        against existance of password, according to ask_pass settings.

        It assumes several things about accepted command arguments:
        * unspecified arguments are None (or don't exist at all) - VERY IMPORTANT!
        * password is stored in configuration["password"]
        * interactive decision about asking for missing password is stored in 
          configuration["ask_pass"]
        """

        section_name, cfg = choose_section(config,
            section_name = args.configuration,
            protocol = self.name,
            field_name = sender_field,
            field_val = getattr(args, sender_field))

        # Command line options override config.
        for arg, val in vars(args).items():
            if val is not None and (arg in cfg.attributes()):
                cfg[arg] = val

        # validators may mutate configuration properties. They may even initialize some fields.
        cfg.validate()

        self.__fill_password(cfg, user_field)
        return cfg

    def _read_message(self, args):
        """Returns formatted message. First args.message is checked, then args.msg_file and finally
        sys.stdin."""

        # when message is provided, it is a list of stripped words, possibly with a newline,
        # but when we read from file or stdin, we get full, formatted lines with newline at the end.
        # That's why we have to format them differently here (as it's the only place that knows the
        # source of message).
        if args.message is not None and len(args.message) > 0:
            # No need to worry here about things like ['a', 'b\n', 'c'] as they are impossible
            # to construct from command line. Please prove if that's incorrect statement.
            return " ".join(args.message)
        elif args.msg_file is not None:
            return "".join(self.__read_file(args.msg_file))
        else:
            print(_("Message (end with EOF):"))
            return "".join(self.__read_stdin())

    def __read_file(self, filename):
        try:
            with open(filename, "r") as f:
                return f.readlines()
        except OSError as e:
            raise ExceptionWithCode(ErrorCode.BAD_INPUT, str(e))

    def __read_stdin(self):
        return sys.stdin.readlines()

    def __fill_password(self, configuration, user_field):
        if user_field is None:
            return

        if configuration.get("password") is None:
            if configuration.get(user_field) is not None and configuration.get("ask_pass") is True:
                configuration["password"] = getpass.getpass(_("Password: "))
