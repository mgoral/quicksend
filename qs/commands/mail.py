#-*- coding: utf-8 -*-
#
# Copyright (C) 2015 Michał Góral.
# 
# This file is part of quicksend
# 
# quicksend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# quicksend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License

import os
import argparse
import logging
import getpass
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication

from qs.commands._interface import ProtocolInterface
from qs.commands._args import prepare_options_from_conf
import qs.senders.mail as mail
from qs.sending import make_sender
from qs.config import Config, choose_section, get_contacts
from qsconf.protocols import mail as mail_conf

from qs.detail.codes import ErrorCode, ExceptionWithCode
from qs.detail.utils import expect
from qs.detail.locale import _

_parser = prepare_options_from_conf("qs mail", mail_conf(None))
_message_options = _parser.add_argument_group(_("message options"))
_message_options.add_argument("--to", action = "append", metavar = _("MAIL"),
    help = _("message recipient(s). May be specified multiple times"))
_message_options.add_argument("--cc", action = "append", metavar = _("MAIL"),
    help = _("carbon copy recipient(s). May be specified multiple times"))
_message_options.add_argument("--bcc", action = "append", metavar = _("MAIL"),
    help = _("blind carbon copy recipient(s). May be specified multiple times"))
_message_options.add_argument("-s", "--subject", metavar = _("STRING"), help = _("message subject"))
_message_options.add_argument("-a", "--attach", action = "append", metavar = _("FILE"),
    help = _("attach a file to a message. May be specified multiple times"))
_message_options.add_argument("-F", "--from-file", dest = "msg_file", metavar = _("FILE"),
    help = _("send message stored in a file"))
_message_options.add_argument("message", metavar = _("MESSAGE"), nargs = "*", help = _("message body"))

log = logging.getLogger("quicksend.%s" % __name__)

class Command(ProtocolInterface):
    def __init__(self):
        super().__init__(name = "mail", description = _("send an e-mail message"))

    def execute(self, config, env, global_args, command_remainder):
        args = _parser.parse_args(command_remainder)
        args.message = self._read_message(args)

        configuration = self._get_configuration(config, args, "sender", "user")
        message = self._get_message(args, configuration)

        if (global_args.verbose is True):
            print(message.as_string())

        ec = ErrorCode.NO_ERROR
        with make_sender(mail.Mail, configuration) as s:
            ec = s.send(message)
        return ec

    @classmethod
    def print_help(self):
        _parser.print_help()

    def _get_message(self, args, configuration):
        def set_recipients_in_header(msg, header_name, recvs):
            if recvs is not None:
                msg[header_name] = ", ".join(get_contacts(configuration, set(recvs)))

        expect(args.to is not None or args.cc is not None or args.bcc is not None,
            ErrorCode.BAD_INPUT, _("Missing recipients"))

        msg = None
        txt = MIMEText(args.message)

        if args.attach is None:
            # Send message as text message instead of multipart when there's no attachment
            msg = txt
        else:
            msg = MIMEMultipart()
            msg.attach(txt)
            self._attach_files(msg, args.attach)

        msg["From"] = configuration.get("sender")

        if args.subject is not None:
            msg["Subject"] = args.subject
        set_recipients_in_header(msg, "To", args.to)
        set_recipients_in_header(msg, "Cc", args.cc)
        set_recipients_in_header(msg, "Bcc", args.bcc)
        return msg

    def _attach_files(self, msg, attachments):
        for attachment in attachments:
            fname = os.path.basename(attachment)
            try:
                with open(attachment, "rb") as f:
                    a = MIMEApplication(f.read())
                    a.add_header('Content-Disposition', 'attachment',
                                 filename='%s' % fname)
                    a.add_header('Name', fname)
                    msg.attach(a)
            except FileNotFoundError:
                # Theoretically we could skip that file, but users probably don't want
                # to be fooled into sending incorrect message with a missing attachment.
                raise ExceptionWithCode(
                    ErrorCode.BAD_INPUT, _("No such file: '%s'. Stopping.") % attachment)
