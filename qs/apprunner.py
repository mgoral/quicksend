#-*- coding: utf-8 -*-
#
# Copyright (C) 2015 Michał Góral.
#
# This file is part of quicksend
#
# quicksend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# quicksend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with quicksend. If not, see <http://www.gnu.org/licenses/>.

import logging
log = logging.getLogger('quicksend')

import os
import sys
import importlib
import operator
import argparse
import pkgutil
import signal

import qs.commands

from qs.config import Config
from qs.environ import Environment
from qs.version import __version__
from qs.detail.codes import ExceptionWithCode, ErrorCode
from qs.detail.locale import _

def get_argparse_epilog(commands):
    justification = len(max(commands.keys(), key=len)) + 2
    descr_list = [ (c.name, c.description) for c in commands.values() ]
    descr_list .sort(key = operator.itemgetter(0))
    clstr = [ "  %s  %s" % (elem[0], elem[1]) for elem in descr_list ]

    # Just don't make these strings too long...
    epilog_header = _("Supported commands are: ")
    help_str = _("See '%(prog)s help <command>' to read about a specific command.")
    return "%s\n%s\n\n%s" % ( epilog_header, "\n".join(clstr), help_str )

def prepare_argparse(commands):
    allowed_commands = ", ".join([c for c in commands])
    parser = argparse.ArgumentParser(
        formatter_class = argparse.RawDescriptionHelpFormatter,
        usage = _("%(prog)s [options] command [command-options]"),
        epilog = get_argparse_epilog(commands),
        add_help = False)

    # Global options, like '-h' or '-v'
    global_options = parser.add_argument_group(_("Global options"))

    global_options.add_argument("-h", "--help", action = "help",
            help = _("show this help message and exit"))
    global_options.add_argument("--version", action = "version",
            version = "%(prog)s %(version)s" % {"prog" : "%(prog)s", "version" : __version__},
            help = _("print program version"))
    global_options.add_argument("-v", "--verbose", action = "store_true",
            help = _("enable verbose printing"))
    global_options.add_argument("--debug", action = "store_true",
            help = _("enable debug printing (including libraries)"))

    command_options = parser.add_argument_group(_("Commands"))
    command_options.add_argument("command", type = str, help = argparse.SUPPRESS)
    command_options.add_argument("command_options", metavar = "command-options",
            nargs = argparse.REMAINDER, help = argparse.SUPPRESS)

    return parser

def handle_logging_verbosity(args):
    fmt = "%(message)s"

    if args.debug:
        fmt = "[%(levelname)s:%(name)s] %(message)s"
        logging.basicConfig(format = fmt, level = logging.DEBUG)
        logging.getLogger("sleekxmpp").setLevel(logging.DEBUG)
    else:
        logging.getLogger("sleekxmpp").setLevel(logging.CRITICAL)

    if args.verbose:
        logging.basicConfig(format = fmt, level = logging.INFO)
    else:
        logging.basicConfig(format = fmt, level = logging.WARNING)

def get_command(mod_name):
    """Returns a Command() from a qs.commands.<mod_name> module or None if there is none.
    Call it a 'template method' ;)"""
    command = None

    py_mod = importlib.import_module("qs.commands.%s" % mod_name)

    expected_class = "Command"
    if hasattr(py_mod, expected_class):
        command = getattr(py_mod, expected_class)()

    return command

def load_commands():
    """Loads all commands from qs.commands.*"""

    commands = {}
    for importer, mod_name, ispkg in pkgutil.iter_modules(qs.commands.__path__):
        if mod_name.startswith("_"):
            continue

        command = get_command(mod_name)
        if command  is None:
            continue
        assert commands.get(command.name) is None, "Cannot load two '%s' commands" % command.name
        commands[command.name] = command

    return commands

def sigint_handler(sig, frame):
    log.debug(_("Interrupted"))
    # https://www.cons.org/cracauer/sigint.html
    signal.signal(sig, signal.SIG_DFL)
    os.kill(os.getpid(), sig)

def main():
    signal.signal(signal.SIGINT, sigint_handler)

    commands = load_commands()
    parser = prepare_argparse(commands)
    commands["help"].store_global_help(parser.print_help)

    args = parser.parse_args()

    handle_logging_verbosity(args)

    command = commands.get(args.command)
    if command is None:
        log.critical(_("Unknown command: %s" % args.command))
        return ErrorCode.BAD_INPUT

    try:
        env = Environment()
        config = Config()
        return command.execute(config, env, args, args.command_options)
    except ExceptionWithCode as e:
        if e.description() is not None:
            log.critical(str(e))
        return e.error_code()
