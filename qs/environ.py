#-*- coding: utf-8 -*- #
# Copyright (C) 2016 Michał Góral.
#
# This file is part of quicksend
#
# quicksend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# quicksend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with quicksend. If not, see <http://www.gnu.org/licenses/>.

import re
import collections
import urllib.request
import urllib.parse

def _unquote(s):
    if s is not None:
        return urllib.parse.unquote(s)
    return s

def _is_netloc(addr):
    """Very simple, but usually suficcent check whether given addr is netloc.
    Netlocs contain either a dot (domains, IPv4) or a colon (IPv6)."""
    return any(ch in addr for ch in ('.', ':'))

Proxy = collections.namedtuple('Proxy', ['host', 'port', 'username', 'password'])

class Environment:
    def __init__(self):
        proxies = urllib.request.getproxies()
        self._http_proxy = self._read_proxy('http', proxies)
        self._https_proxy = self._read_proxy('https', proxies)
        self._no_proxy = self._read_no_proxy(proxies)

    @property
    def http_proxy(self):
        """Returns parsed environment's http_proxy or None if it isn't set."""
        return self._http_proxy

    @property
    def https_proxy(self):
        """Returns parsed environment's http_proxy or None if it isn't set."""
        return self._https_proxy

    @property
    def no_proxy(self):
        """Returns parsed as a list environment's no_proxy or None if it isn't
        set."""
        return self._no_proxy

    def _read_proxy(self, proxy_type, proxies):
        proxy = proxies.get(proxy_type)
        if proxy is None:
            return None
        return self._parse_proxy(proxy)

    def _read_no_proxy(self, proxies):
        no_proxy = proxies.get('no')
        if no_proxy is not None:
            ret = []
            for np in no_proxy.split(','):
                np = _unquote(np.strip())
                # Small hack, but this is how no_proxy actually works in
                # programs like curl and wget. It enables us to correctly match
                # domains and addresses when subdomain is given. The only
                # question is how we should handle glob expressions already
                # given by user (see comment in test_no_proxy_glob). Currently
                # we check only the first character, but alternatively we might
                # not add '*' when np already contains it anywhere.
                if _is_netloc(np) and not np.startswith('*'):
                    np = '*%s' % np
                ret.append(np)
            return ret
        return []

    def _parse_proxy(self, proxy):
        # spec for parsing: https://tools.ietf.org/html/rfc1808.html
        o = urllib.parse.urlparse(proxy)
        return Proxy(username=_unquote(o.username),
                     password=_unquote(o.password),
                     host=_unquote(o.hostname),
                     port=o.port if o.port else 1080)
