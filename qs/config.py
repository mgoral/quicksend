#-*- coding: utf-8 -*- #
# Copyright (C) 2015 Michał Góral.
#
# This file is part of quicksend
#
# quicksend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# quicksend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with quicksend. If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import importlib
import traceback

from qsconf._protocol_base import ProtocolConfiguration, IncorrectConfiguration
from qsconf.protocols import mail, xmpp
from qs.detail.codes import ErrorCode
from qs.detail.version import __appname__
from qs.detail.codes import ExceptionWithCode, ErrorCode
from qs.detail.locale import _

class _GuardBytecodeSetting:
    def __init__(self, s):
        self._s = s
        self._old = sys.dont_write_bytecode

    def __enter__(self):
        sys.dont_write_bytecode = self._s
        return self

    def __exit__(self, type, value, traceback):
        sys.dont_write_bytecode = self._old

def _home():
    """Return a home directory for a current user."""
    return os.path.expanduser("~")

def _get_env_path(var, default = None):
    """Returns a list of paths specified in a given environment variable.
    Paths set in an environment variable  may be splitted by a colon sign (':')."""

    if default is None:
        default = []

    env = os.getenv(var)

    if env is not None and env != "":
        return env.split(":")
    else:
        return default

def _get_config_search_paths():
    """XDG Base Directory search, according to this spec:
    http://standards.freedesktop.org/basedir-spec/basedir-spec-latest.html"""

    config_home = _get_env_path("XDG_CONFIG_HOME", [os.path.join(_home(), ".config")])
    config_dirs = _get_env_path("XDG_CONFIG_DIRS", ["/etc/xdg"])

    # XDG_CONFIG_HOME is the most important directory and should be searched before anything
    return config_home + config_dirs

def _find_qsrc_in_traceback(exc_traceback):
    extracted_traceback = traceback.extract_tb(exc_traceback)
    for frame in extracted_traceback:
        if frame[0].endswith("qsrc.py"):
            return frame
    return None

def _find_config():
    """Find a configuration file from one of the directories specified by Freedesktop standard.
    Search order is the following:
    1. $XDG_CONFIG_HOME/quicksend/qsrc (or ~/.config/quicksend/qsrc)
    2. each of $XDG_CONFIG_DIRS/quicksend/qsrc (or /etc/xdg/quicksend/qsrc)"""

    def exists(path):
        try:
            with open(path):
                return True
        except IOError:
            return False

    search_dirs = _get_config_search_paths()
    for config_dir in search_dirs:
        rcfile = os.path.join(config_dir, "%s/qsrc.py" % __appname__)
        if exists(rcfile):
            return rcfile

    return None

def _read_config(cfgpath):
    """Reads a given cfgpath and returns initialised dictionary."""

    ret = dict()

    if cfgpath is None:
        return ret

    with _GuardBytecodeSetting(True):
        module_name = os.path.splitext(os.path.basename(cfgpath))[0]
        dirname = os.path.dirname(cfgpath)

        try:
            l = importlib.find_loader(module_name, [dirname])
            module = l.load_module()
        except Exception as e:
            _1, _2, exc_traceback = sys.exc_info()

            qsrc_frame = _find_qsrc_in_traceback(exc_traceback)

            # Unlikely, but...
            if qsrc_frame is None:
                raise

            msg = str(e)
            filepath = qsrc_frame[0]
            line_no = qsrc_frame[1]

            raise ExceptionWithCode(ErrorCode.BAD_INPUT,
                _("%(file)s, line %(line)d: %(msg)s") % dict(file=filepath, line=line_no, msg=msg))

        configuration = {}

        for user_config in ProtocolConfiguration.instances():
            if user_config._section_name in configuration:
                raise IncorrectConfiguration(_("%s used twice") % user_config._section_name)

            configuration[user_config._section_name] = user_config

        return configuration

def field_matches(section, field, field_val):
    """A predicate to be used in Config.find_sections.
    Returns True when a given section has a field with a value set to field_val."""
    val = section.get(field)
    return val is not None and val == field_val

def is_protocol(section, protocol):
    """A predicate to be used in Config.find_sections.
    Returns True when a given section's protocol matches a given one."""
    return field_matches(section, "protocol", protocol)

def is_protocol_with_field(section, protocol, field, field_val):
    """A predicate to be used in Config.find_sections.
    Returns True when a given section's protocol matches and a given field is set to a given
    field_val."""
    return is_protocol(section, protocol) and field_matches(section, field, field_val)

class Config:
    def __init__(self, rcfile=None):
        if rcfile is not None:
            self._rcfile = rcfile
        else:
            self._rcfile = _find_config()

        self._cfg = _read_config(self._rcfile)

    def path(self):
        """Returns a file path to the used configuration file. Might be None if there's no
        configuration file found."""
        return self._rcfile

    def get_section(self, section, default = None):
        """Returns a user-defined configuration"""
        try:
            return self._get_section(section)
        except KeyError:
            return default

    def find_sections(self, predicate):
        """Returns names of sections for which a call to predicate(config_section) returns
        True."""

        ret = []

        sections = self._sections()
        for s in sections:
            if predicate(self._get_section(s)) is True:
                ret.append(s)

        return ret

    def has_section(self, name):
        return name in self._sections()

    def _sections(self):
        """Returns a list of sections in config file"""
        return list(self._cfg.keys())

    def _get_section(self, section):
        """Returns a section"""
        return self._cfg[section]

def choose_section(config, section_name, protocol, field_name, field_val):
    """Helper function that returns the exact section which meets the given criteria: either section
    name or protocol with a field and field_val. Usually it's used to deduce a section name with a
    partial data provided by a user via command line.

    Except 'config', each of accepted arguments can be None, but success is not guaranteed in such
    case. That's why we call it "deduction". ;)

    Throws ExceptionWithCode if no or more than one section meets the given criteria."""

    def _get_section_tuple(name):
        if name is None:
            return (None, eval("%s(None)" % protocol))
        return (name, config.get_section(name))

    # First try: explicitly given section is our best option.
    if section_name is not None:
        if config.has_section(section_name):
            return _get_section_tuple(section_name)
        raise ExceptionWithCode(ErrorCode.BAD_INPUT,
            _("No configuration named '%s'") % section_name)

    # Second try: find a given field within the given protocol sections
    if protocol is not None and field_val is not None and field_name is not None:
        sections = config.find_sections(
            lambda s, p=protocol, f=field_name, fv=field_val: is_protocol_with_field(s, p, f, fv))

        if len(sections) == 1:
            return _get_section_tuple(sections[0])
        elif len(sections) > 1:
            raise ExceptionWithCode(
                ErrorCode.BAD_INPUT,
                _("Ambiguous configuration for '%(protocol)s' with '%(name)s = %(val)s'. Try using"
                "'-c' switch.") % dict(protocol=protocol, name=field_name, val=field_val))

    # Third try: if there's only one section for a given protocol, use it
    if protocol is not None and (field_val is None or field_name is None):
        sections = config.find_sections(lambda s, p=protocol: is_protocol(s, p))

        if len(sections) == 1:
            return _get_section_tuple(sections[0])
        elif len(sections) > 1:
            raise ExceptionWithCode(ErrorCode.BAD_INPUT,
                _("Can't choose a single configuration for '%s'") % protocol)

    if protocol is None:
        raise ExceptionWithCode(ErrorCode.BAD_INPUT, _("Cannot determine configuration"))
    return _get_section_tuple(None)


def get_contacts(config_section, contact_names):
    """Fetches address stored in a given config_section for each contact in contact_names.  If
    section is None or contact isn't stored in it, returns a contact_name in its place instead."""

    if config_section is None or config_section.get("contacts") is None:
        return contact_names

    contacts = config_section.get("contacts")

    if contacts is None:
        return contact_names

    return [ contacts.get(contact, contact) for contact in contact_names ]
