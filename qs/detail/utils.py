#-*- coding: utf-8 -*-
#
# Copyright (C) 2015 Michał Góral.
# 
# This file is part of quicksend
# 
# quicksend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# quicksend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with quicksend. If not, see <http://www.gnu.org/licenses/>.

import sys
import fnmatch
import urllib.parse
from qs.detail.codes import ExceptionWithCode

def expect(cond, ec, desc):
    if bool(cond) is False:
        raise ExceptionWithCode(ec, desc)

def choose_proxy(env, host):
    """Smartly chooses proxy. Will first use https_proxy if it's appropriate and
    http_proxy otherwise. If http_proxy is not set, will fall back to
    https_proxy. Returns None if no proxy should be used."""
    if host and env.no_proxy:
        parsed = urllib.parse.urlparse(host)
        temp_host = host  # don't modify original host
        if parsed.hostname:
            temp_host = parsed.hostname
        if any(fnmatch.fnmatch(temp_host, elem) for elem in env.no_proxy):
            return
    if host and host.startswith("https://") and env.https_proxy:
        return env.https_proxy
    if env.http_proxy:
        return env.http_proxy
    if env.https_proxy:
        return env.https_proxy
    return

def find(pred, iterable):
    """Finds and returns element which matches the given predicate. Raises
    LookupError if no element is found."""
    for elem in iterable:
        if pred(elem):
            return elem
    raise LookupError("element matching a predicate not found")
