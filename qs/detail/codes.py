#-*- coding: utf-8 -*-
#
# Copyright (C) 2015 Michał Góral.
# 
# This file is part of quicksend
# 
# quicksend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# quicksend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with quicksend. If not, see <http://www.gnu.org/licenses/>.

class ErrorCode:
    NO_ERROR = 0
    FATAL_ERROR = 1
    BAD_INPUT = 2
    SEND_ERROR = 10
    NO_CONNECT = 11

class ExceptionWithCode(Exception):
    def __init__(self, code, description = None):
        super().__init__(code, description)

    def error_code(self):
        return self.args[0]

    def description(self):
        return self.args[1]

    def __str__(self):
        if self.description() is None:
            return str()
        return str(self.description())
