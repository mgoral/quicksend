#-*- coding: utf-8 -*-
#
# Copyright (C) 2015 Michał Góral.
# 
# This file is part of quicksend
# 
# quicksend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# quicksend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with quicksend. If not, see <http://www.gnu.org/licenses/>.

import os
import gettext

localedir = os.path.join ("/usr", "share", "locale")
gettext.bindtextdomain("quicksend", localedir)
gettext.textdomain("quicksend")

t = gettext.translation(
    domain="quicksend",
    localedir=localedir,
    fallback=True)

_ = t.gettext
P_ = t.ngettext

