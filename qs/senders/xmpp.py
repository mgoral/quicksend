#-*- coding: utf-8 -*-
#
# Copyright (C) 2015 Michał Góral.
# 
# This file is part of quicksend
# 
# quicksend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# quicksend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with quicksend. If not, see <http://www.gnu.org/licenses/>.

import sys
import logging

import sleekxmpp

from qs.sending import Sender
from qs.async import EventVariable
from qs.detail.codes import ErrorCode, ExceptionWithCode
from qs.detail.locale import _
from qs.detail.utils import choose_proxy, find

log = logging.getLogger("quicksend.%s" % __name__)

class XMPP(Sender):
    def __init__(self, configuration, env):
        super().__init__(configuration)

        jid = configuration.get("jid")
        self._client = sleekxmpp.ClientXMPP(jid, configuration.get("password", ""))

        self._client.add_event_handler('session_start', self._session_start_handler)
        self._client.add_event_handler('session_end', self._session_end_handler)
        self._client.add_event_handler('disconnected', self._disconnected_handler)
        self._client.add_event_handler('failed_auth', self._failed_auth_handler)

        self._env = env
        self._session_started = EventVariable(False)

    def connect(self):
        cfg = self.configuration

        host = cfg.get("host")
        port = cfg.get("port")
        security = cfg.get("security")

        kw = { "reattempt" : False }
        if host is not None and port is not None:
            kw["address"] = (host, int(port))
        if security is not None:
            if security == "ssl":
                kw["use_ssl"] = True
                kw["use_tls"] = False
            elif security == "tls":
                kw["use_ssl"] = False
                kw["use_tls"] = True
        else:
                kw["use_ssl"] = False
                kw["use_tls"] = False

        self._enable_proxy()
        self._client.connect(**kw)
        self._client.process(block=False)
        timeout = 30.0
        return self._wait_for_connection(timeout)

    def disconnect(self):
        self._client.disconnect(wait = True)
        self._session_started.set_var(False)
        self._session_started.clear_lock()
        self._client.set_stop()

    def send(self, msg, recipients):
        for recipient in recipients:
            self._client.send_message(mto = recipient, mbody = msg, mtype = "chat")
        return ErrorCode.NO_ERROR

    def _wait_for_connection(self, timeout):
        cond = self._session_started.wait(timeout)
        if cond is not True:
            log.error(_("Couldn't start XMPP session"))
            return False
        return True

    def _session_start_handler(self, ev):
        log.debug("Session started")
        self._client.send_presence()
        self._client.get_roster()
        self._session_started.set_var(True)

    def _session_end_handler(self, ev):
        self._session_started.set_var(False)
        log.debug("Session stopped")

    def _disconnected_handler(self, ev):
        self._session_started.set_var(False)
        log.debug("Disconnected")

    def _failed_auth_handler(self, ev):
        self._session_started.set_var(False)
        log.error(_("Authentication failed"))

    def _enable_proxy(self):
        cfg = self.configuration
        host = cfg.get('host')
        jid = cfg.get('jid')
        proxy_settings = choose_proxy(self._env, self._proxy_host(host, jid))
        if proxy_settings:
            self._client.use_proxy = True
            self._client.proxy_config['host'] = proxy_settings.host
            self._client.proxy_config['port'] = proxy_settings.port
            self._client.proxy_config['username'] = ''
            self._client.proxy_config['password'] = ''
            if proxy_settings.username:
                self._client.proxy_config['username'] = proxy_settings.username
            if proxy_settings.password:
                self._client.proxy_config['password'] = proxy_settings.password

    def _proxy_host(self, host, jid):
        if '@' in jid:
            try:
                return find(lambda h: h is not None,
                            [host, jid.rpartition('@')[-1]])
            except LookupError:
                pass
        return host
