#-*- coding: utf-8 -*-
#
# Copyright (C) 2015 Michał Góral.
# 
# This file is part of quicksend
# 
# quicksend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# quicksend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with quicksend. If not, see <http://www.gnu.org/licenses/>.

import smtplib
import logging
import socket

from qs.sending import Sender
from qs.detail.codes import ErrorCode, ExceptionWithCode
from qs.detail.locale import _

log = logging.getLogger("quicksend.%s" % __name__)

class Mail(Sender):
    def __init__(self, configuration):
        super().__init__(configuration)

        self._client = None

    def connect(self):
        cfg = self.configuration

        host = cfg.get("host")
        port = cfg.get("port")
        user = cfg.get("user")
        password = cfg.get("password", "")
        security = cfg.get("security", "")

        try:
            timeout = 30.0
            if security == "ssl":
                self._client = smtplib.SMTP_SSL(host, port, timeout=timeout)
            else:
                self._client = smtplib.SMTP(host, port, timeout=timeout)

            if security == "starttls":
                self._client.starttls()

            if user is not None:
                self._client.login(user, password)
        except socket.timeout:
            log.error(_("Connection timeout"))
            return False
        except ConnectionRefusedError:
            log.error(_("Connection refused. Check credentials"))
            return False
        except smtplib.SMTPException as e:
            log.error(str(e))
            return False
        return True

    def disconnect(self):
        self._client.quit()

    def send(self, msg):
        try:
            self._client.send_message(msg)
        except smtplib.SMTPException as e:
            raise ExceptionWithCode(ErrorCode.SEND_ERROR, str(e))
        return ErrorCode.NO_ERROR

