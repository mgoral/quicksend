#-*- coding: utf-8 -*-
#
# Copyright (C) 2015 Michał Góral.
# 
# This file is part of quicksend
# 
# quicksend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# quicksend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with quicksend. If not, see <http://www.gnu.org/licenses/>.

import threading

class EventVariable:
    """Locking mechanism waiting for variable changes.
    Basically it differs from threading.Event() with returning undrlying variable from call to
    wait()."""

    def __init__(self, default = None):
        self._var = default
        self._lock = threading.Event()

    def clear_lock(self):
        """Clears the internal lock, enabling EventVariable to signalise another variable change."""
        self._lock.clear()

    def set_var(self, val):
        """Changes stored variable. All waiting threads are awakened. Threads that call wait() after
        set_var() will not block at all. To make them block again, clear_lock() must be called."""
        self._var = val
        self._lock.set()

    def get_var(self):
        """Returns stored variable."""
        return self._var

    def wait(self, timeout = None, timeout_ret = None):
        """Blocks until variable is changed in the other thread. If variable was changed beforehand,
        it returns immediately. If timeout occurs, 'timeout_ret' is returned."""

        if self._lock.wait(timeout) is False:
            return timeout_ret

        return self._var
