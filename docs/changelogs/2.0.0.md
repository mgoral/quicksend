# Quicksend 2.0.0 changelog

## Changes since 1.0.2:

### New features

* Specify all configuration parameters on command line

    Added a possibility to specify all configuration parameters available in
    `qsrc.py` as command line parameters. Because of this, existance of
    `qsrc.py` is no longer necessary.

    Backward incompatible changes:
    - removed short form of xmpp's jid option (-j)
    - don't return default (single) configuration when sender (--sender, --jid)
      doesn't match

* Accept messages from files and stdin.

    Messages can be passed to quicksend via file (-F and --from-file switches
    for XMPP and mail protocol commands) and stdin (they can be piped in from
    other command's output or typed in interactively).

    This is interface change as quicksend will now accept empty MESSAGE strings
    (previously it'd return an error).

* Send XMPP messages through a proxy server.

    If system is configured to use HTTP proxy server to forward all requests,
    XMPP messages will use it (i.e. `http_proxy`, `https_proxy` and `no_proxy`
    environment variables are respected).

### Bug fixes

* Configuration deduction throws better, more descriptive errors when it's
  unsuccessful.

* qsrc.py errors are communicated in a user friendly way.

    Only a single line is displayed. It contains qsrc.py path, line number in
    qsrc on which error occured and message of the error (exception string).

* Fix problems with mail attachments with names which should be escaped.

* Fix proper exit statuses.

### Other changes

* Rephrase some sections of qsrc.py manual. Slightly changed branch naming
  policy.

* `make install` now creates a virtualenv with all dependencies at
  $PREFIX/share (virtualenv is now a mandatory dependency).

    A wrapper script is created in `$PREFIX/bin`, which starts quicksend via
    virtualenv. This resolves problems with having incompatible versions of some
    libs (like dnspython and sleekxmpp).

* Checking if sleekxmpp is available has been removed (because `make install`
  now assures that it is).

* When quicksend waits for interactive message body, it indicates it by printing
  a message to the user.
