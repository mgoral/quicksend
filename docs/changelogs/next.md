# Quicksend next changelog

## Changes since 2.0.0:

### New features

* `qs show-config` accepts an optional configuration name. If given, it'll
  print all set entries for that configuration. Passwords are obfuscated by
  default, but they can be printed when `--with-passwords` option is set.

### Bug fixes

* Fixed crash when proxy without authentication is used
