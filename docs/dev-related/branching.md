## Branching name conventions

All public (remote) branches must follow the following convention:

    branch-prefix/branch-name

`branch-name` should be short and descriptive, preferably 1-2 words.

`branch-prefix` is one of the following:

    feat - for feature branches
    bug  - for branches with bugfixes
    impr - for branches with internal improvement (like refactoring)
    rel  - for (potential) release candidates

Examples:

    feat/sending-emails
    bug/bad-cli-parsing
    impr/class-refa
    rel/1.0.1
