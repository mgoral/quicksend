% qs-xmpp(1) @PACKAGE_NAME@ @VERSION@ | Quicksend Manual
%
% @FILE_LAST_MODIFY@

# NAME

qs-xmpp - send XMPP messages

# SYNOPSIS

qs [options]... xmpp [command-options] [*message*]

# DESCRIPTION

Sends XMPP *message* to specified recipients.

If message body is provided as a remainder of this command, it is advised to
quote it. Otherwise it can be read from file contents or can be piped into
quicksend from the other command's output. If no message is provided, quicksend
will wait for interactive input. It's terminal-specific, but usually end of
input is signalled by several presses of `ctrl-d`.

# COMMAND OPTIONS

## Configuration options

Beside `--configuration` all long options in this subsection can be put in
`qsrc.py` file. 

-c, --configuration = *name*
:   use XMPP specific configuration read from `qsrc.py`. It is useful if there
are many `xmpp` configurations and `--jid` is not used.  If it's used together
with `--jid`, specified configuration will be used, even if there's another one
matching a given *jid*.

--jid, qsconf.protocols.xmpp.jid = *jid*
:   uses a given *jid* as a sender of a message. If specific configuration is
not set with `-c` option, quicksend will try to find an entry in `qsrc.py`
which matches the given *jid* and fill from it otherwise missing parameters.

--password, qsconf.protocols.xmpp.password = *string*
:   password used to log in to the given host. Remember that it's insecure to
provide a password via a plain-text option like that. A better option is to use
a password manager and query it in `qsrc.py` file. Examples of how to do that
can be found in *howto/qsrc.py.example*.

--host, qsconf.protocols.xmpp.host = *address*
:   name of the host. If it's set, it will override DNS lookup which may be
performed for a given `jid`. Because of that it is advised to set this field
only if setting `jid` is not enough to connect to a wanted host.

--port, qsconf.protocols.xmpp.port = *number*
:   port on which host is listening. It's a mandatory field whenever `host` is
set.

--security, qsconf.protocols.xmpp.security = *ssl*|*tls*
:   tells if TLS or SSL should be used for the connection. If not specified, no
security mode will be used.

--ask_pass, qsconf.protocols.xmpp.ask_pass = *True*|*False*
:   tells if quicksend should ask for a password when it's not specified by a
*password* field. If it is not set, quicksend will ask for passwords.

## Message options

-F, --from-file = *file*
:   use contents of *file* as a message body.

--to =* recipent*
:   *message* recipient. Can be used multiple times, creating a list of
recipients to which *message* will be sent.

## Other options

-h, --help
:    shows a brief summary of available command options.

# PROXY

XMPP messages can be sent through proxy. System proxy configuration is used:
`http_proxy` and `https_proxy` environment variables are read and used. Messages
are not sent through proxy when XMPP host is found in a comma-separated list in
`no_proxy` environment variable. Glob expressions are allowed in `no_proxy`, so
it means that both **.mit.edu** and **\*.mit.edu** will prevent using proxies
when sending messages to MIT.

# EXAMPLES

    x = xmpp("gtalk")
    x.protocol = "xmpp"
    x.jid = "my-account@gmail.com
    x.password = lazy_exec("pass accounts/gtalk")
    x.contacts = { "Alice" : "alice@example.com", "Bob" : "bob21@example.com" }

$ qs xmpp --to Alice --to mum@mums.org "Hello, I'm testing a new, cool program!"

$ qs xmpp --to Bob -F ~/message-to-bob.txt

$ qs xmpp -c gtalk --password="Different password" "Hello, recently I changed my password"

$ qs xmpp --jid=my-account@gmail.com --password="Different password" "Hello, recently I changed my password"

$ qs xmpp --jid=other-xmpp@example.com --password=pass --security=tls "Hello"

$ echo "Hello Charlie, are you OK?" | qs xmpp --to Charlie -c otherXMPPaccount

# SEE ALSO

qsrc.py(5), *docs/howto/qsrc.py.example*

<!-- vim: set tw=80 : -->
