# This is an example qsrc.py configuration file. You can modify it to your
# liking.  When you're done, put it to ~/.config/quicksend/qsrc.py.
#
# qsrc.py is just an ordinary Python file. It is imported in the context of
# quicksend, that's why it has access to 'qsconf' package, which contains all
# data structures needed to configure quicksend. Mostly these are some classes
# representing different protocols supported by quicksend, but there are also
# some helper functions.
#
# The easiest way to learn what's available is probably writing
# 'print(dir(qsconf)) after the import statements. You can also check out
# 'man qsrc.py' which has a reference for qsconf package.
#
# Don't be scared. Even if you're not a programmer, you won't find qsrc.py very
# different from most configuration formats. It was designed for maximum
# simplicity, but still with "power-users" in mind.

#
# This is required. Module 'qsconf.protocols' contains basic configuration
# structures allowing configuring credentials for each supported protocols.
from qsconf.protocols import mail, xmpp

#
# Quicksend also comes with some handy functions. One of them is
# 'lazy_exec' from 'qsconf.process' module. It allows lazy execution of of shell
# commands. Normally each statement in qsrc.py is executed once the file is
# read, but 'lazy_exec' allows postponing shell commands to the point where
# they're really needed. This can save us a lot of time on quicksend's startup.
from qsconf.process import lazy_exec

#
# This is our address book. We'll specify it here, so we can reuse it for both
# GTalk and GMail, which generally use the same addresses. Remember that contact
# names are case-sensitive.
my_contacts = {
    "Alice" : "alice@gmail.com",
    "Bob" : "bob@gmail.com",
    "Charlie" : "spam+charlie@gmail.com",
}

#
# All credentials are stored in separate configuration objects each for a
# specific protocol imported from 'qsconf.protocols' module. Each protocol must
# be given a name under which it'll be known.  It can be referenced e.g. by
# running qs with '-c' option.  Let's say we want to configure some mail
# account. It should be done this way:
#
#   anything = mail("my-name")
#   aything.host = smtp.example.com
#
# Then we can choose it this way (or let quicksend to deduce which configuration
# should be used):
#
#   $ qs mail -c my-name [... remaining options ...]

#
# Let's start with configuring our GTalk account:
gtalk = xmpp("gtalk")

#
# We need to tell quicksend about our credentials (Jabber ID and password).
# We'll get our password from secret-tool, a nice program used to access user
# keyrings. On most systems it's part of 'libsecret-tools' package. We'll also
# use 'lazy_exec' for this, so password will be fetched only when it's really
# needed (i.e. only when you want to send a message from your GTalk account).
gtalk.jid = "your-account@gmail.com"
gtalk.password = lazy_exec("secret-tool lookup username your-account@gmail.com")

#
# If pythondns is not installed you'll have to manualy specify the host:
#gtalk.host = "xmpp.l.google.com"

#
# This tells which connection method shoud be used. For GTalk defaults are just
# fine
#gtalk.security = "tls"
#gtalk.security = "ssl"

#
# We add our address book for GTalk, so we can use something like this:
#
#   $ qs xmpp --to Alice 'Hi, Alice!'
gtalk.contacts = my_contacts



#
# Now we'll configure GMail's SMTP account, which is needed for sending e-mails.
# Note that its name is different than name of object in qsrc.py
# (gmail != gmail.com). qsrc.py's object name doesn't matter however, it can be
# only accessed with "gmail.com":
#
#   $ qs mail -c gmail.com [... remaining options ...]
gmail = mail("gmail.com")

#
# Each mail message must have a sender. You don't have to set it here, but
# otherwise you'd have to set it each time with '--sender' parameter, which is
# not that handy.
gmail.sender = "virgoerns@gmail.com"

#
# Now it's time to tell about real credentials. First username and password for
# GMail SMTP server. For demonstration purposes we'll supply password as a plain
# text instead of fetching it from keyring, like we did for GTalk.
gmail.user = "virgoerns@gmail.com"
gmail.password = "your-account-password"

#
# Now we set server address and port. Default port for SMTP is 25, but Google
# uses port 465, so we have to change it here:
gmail.host = "smtp.googlemail.com"
gmail.port = "465"

#
# Some SMTP servers use secure connection. Two common methods are ssl and
# starttls. Google uses 'ssl':
gmail.security = "ssl"

#
# The same contact list as for GTalk
gmail.contacts = my_contacts

#
# Finally we'll ask quicksend to not ask us about password here. It might
# prevent us from sending any e-mails if 'password' field is not provided.
gmail.ask_pass = "false"



#
# For demonstration purposes we will configure the second mail account. Suppose
# it's on 'example.com' server:
second_mail = mail("example.com")
second_mail.sender = "john@example.com"
second_mail.user = second_mail.sender
second_mail.host = "smtp.example.com"

#
# By default 'lazy_exec' doesn't support doesn't support things like 'piping',
# executing sub-commands in subshells (command $(subcommand)) etc. This is to
# prevent so-called "shell injection attacks". Imagine what would happen if
# subcommand would return some malicious output like ";rm -rf $HOME".
#
# But it's still handy to have these features, e.g. when you're executing only
# trusted subcommands or you don't do that at all. You can enable them by
# passing 'unsafe = True' as a second argument of 'lazy_exec', just like it's
# done below:
second_mail.password = lazy_exec("pass show email/example.com | head -n 1", unsafe = True)

# And that's all folks! :)
#
# vim: set tw=80
