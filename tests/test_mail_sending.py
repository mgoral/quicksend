#-*- coding: utf-8 -*- #
# Copyright (C) 2015 Michał Góral.
#
# This file is part of quicksend
#
# quicksend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# quicksend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with quicksend. If not, see <http://www.gnu.org/licenses/>.

import os
import pytest
import unittest.mock as mock
import copy

import smtplib
import email.mime
import socket

from qs.apprunner import main
from qs.detail.codes import ErrorCode
import qs.senders.mail

import qsconf.protocols as qsp

full_blown_config = qsp.mail("full_blown_config")
full_blown_config.sender = "user@example.com"
full_blown_config.host = "localhost"
full_blown_config.port = 5511
full_blown_config.security = "STARTTLS"
full_blown_config.user = "UserName"
full_blown_config.password = "H4x00r$+yl3 p455\/\/-rD"
full_blown_config.ask_pass = "false"
full_blown_config.contacts = { "Daniel" : "daniel@example.com" }

minimal_config = qsp.mail("minimal_config")
minimal_config.sender = "username"
minimal_config.host = "localhost"

def raise_(exception):
    raise exception

def assert_on_list(val, ok_list, not_ok_lists):
    assert val in ok_list
    for l in not_ok_lists:
        assert val not in l

def get_contact_list(msg, header):
    h = msg[header]
    if h is not None:
        return h.split(", ")
    return []

def get_sent_message(smtp_mock):
    a, kw = smtp_mock.send_message.call_args
    return a[0]

@pytest.fixture
def fake_connection(monkeypatch):
    class MockFactory:
        def __init__(self):
            self.one_mock_per_test_allowed = False

        def get_mock(self):
            return self._get_mock("SMTP")

        def get_mock_ssl(self):
            return self._get_mock("SMTP_SSL")

        def _get_mock(self, name):
            assert self.one_mock_per_test_allowed is False

            orig_class = getattr(smtplib, name)
            ret = mock.create_autospec(orig_class)
            ret.return_value = ret
            monkeypatch.setattr("qs.senders.mail.smtplib.%s" % name, ret)

            self.one_mock_per_test_allowed = True

            return ret

    return MockFactory()

@pytest.fixture
def no_connection(fake_connection):
    m = fake_connection.get_mock()
    m.side_effect = lambda *a, **kw : raise_(ConnectionRefusedError())
    return m

@pytest.fixture
def establish_connection(fake_connection):
    m = fake_connection.get_mock()
    return m

@pytest.fixture
def establish_connection_ssl(fake_connection):
    m = fake_connection.get_mock_ssl()
    return m


#############################################################################################
## TESTS START HERE
#############################################################################################

@pytest.mark.integration
@pytest.mark.mail
@pytest.mark.parametrize("configuration", [full_blown_config, minimal_config])
def test_basic_send_scenario(configuration, fake_config, establish_connection, args):
    fake_config.use_section(configuration)
    args.set_args("qs mail --sender %s --to Alice msg body" % configuration.sender)

    ret = main()
    assert ret == ErrorCode.NO_ERROR

    establish_connection.send_message.assert_called_once_with(mock.ANY)
    establish_connection.quit.assert_called_once_with()

@pytest.mark.integration
@pytest.mark.mail
@pytest.mark.parametrize("configuration", [full_blown_config, minimal_config])
def test_missing_recipient(configuration, fake_config, establish_connection, args):
    fake_config.use_section(configuration)
    args.set_args("qs mail --sender me msg body")

    ret = main()
    assert ret == ErrorCode.BAD_INPUT

@pytest.mark.integration
@pytest.mark.mail
@pytest.mark.parametrize("configuration", [full_blown_config, minimal_config])
def test_message_from_stdin(configuration, fake_config, establish_connection, fake_stdin, args):
    fake_config.use_section(configuration)
    fake_stdin.in_lines = "Hello, this is stdin test!"
    args.set_args("qs mail --to Alice --sender %s" % configuration.sender)

    ret = main()
    assert ret == ErrorCode.NO_ERROR

    msg = get_sent_message(establish_connection)
    to_list = get_contact_list(msg, "To")
    cc_list = get_contact_list(msg, "Cc")
    bcc_list = get_contact_list(msg, "Bcc")

    assert msg.get_payload() == "Hello, this is stdin test!"
    assert_on_list("Alice", to_list, [cc_list, bcc_list])

@pytest.mark.integration
@pytest.mark.mail
@pytest.mark.parametrize("configuration", [full_blown_config, minimal_config])
def test_message_from_file(configuration, fake_config, establish_connection, tmpdir, args):
    fake_config.use_section(configuration)

    f = tmpdir.mkdir("fileinput_test").join("input.txt")
    f.write("Hello, this is the test of mail file input!")

    args.set_args("qs mail --to Alice --sender %s -F %s" % (configuration.sender, f.realpath()))

    ret = main()
    assert ret == ErrorCode.NO_ERROR

    msg = get_sent_message(establish_connection)
    to_list = get_contact_list(msg, "To")
    cc_list = get_contact_list(msg, "Cc")
    bcc_list = get_contact_list(msg, "Bcc")

    assert msg.get_payload() == "Hello, this is the test of mail file input!"
    assert_on_list("Alice", to_list, [cc_list, bcc_list])

@pytest.mark.integration
@pytest.mark.mail
@pytest.mark.parametrize("configuration", [full_blown_config, minimal_config])
def test_missing_configuration(configuration, fake_config, establish_connection, args):
    fake_config.use_section(configuration)
    args.set_args("qs mail --to Alice --sender me -c doesnt-exist msg body")

    ret = main()
    assert ret == ErrorCode.BAD_INPUT
    assert establish_connection.called is False

@pytest.mark.integration
@pytest.mark.mail
@pytest.mark.parametrize("configuration", [full_blown_config, minimal_config])
def test_sending_to_multiple_recipients(configuration, fake_config, establish_connection, args):
    fake_config.use_section(configuration)
    args.set_args("qs mail -c %(sname)s -s SubJect --to Alice --sender iron-man@example.com msg body \
        --to Alice --to Bob --cc Charlie --cc Derek --bcc Eliott --bcc Frank" %
        dict(sname=configuration._section_name))

    ret = main()
    assert ret == ErrorCode.NO_ERROR
    establish_connection.send_message.assert_called_once_with(mock.ANY)

    msg = get_sent_message(establish_connection)

    to_list = get_contact_list(msg, "To")
    cc_list = get_contact_list(msg, "Cc")
    bcc_list = get_contact_list(msg, "Bcc")

    assert msg["From"] == "iron-man@example.com"
    assert msg["Subject"] == "SubJect"
    assert msg.get_payload() == "msg body"

    assert_on_list("Alice", to_list, [cc_list, bcc_list])
    assert_on_list("Bob", to_list, [cc_list, bcc_list])

    assert_on_list("Charlie", cc_list, [to_list, bcc_list])
    assert_on_list("Derek", cc_list, [to_list, bcc_list])

    assert_on_list("Eliott", bcc_list, [cc_list, to_list])
    assert_on_list("Frank", bcc_list, [cc_list, to_list])

@pytest.mark.integration
@pytest.mark.mail
def test_sending_to_contact(fake_config, establish_connection, args):
    fake_config.use_section(full_blown_config)
    args.set_args("qs mail msg body --to Daniel")

    ret = main()
    assert ret == ErrorCode.NO_ERROR

    establish_connection.send_message.assert_called_once_with(mock.ANY)

    msg = get_sent_message(establish_connection)
    assert "daniel@example.com" == msg["To"]

@pytest.mark.integration
@pytest.mark.mail
def test_timeout(fake_config, establish_connection, args):
    fake_config.use_section(full_blown_config)
    args.set_args("qs mail --to Alice msg body")

    establish_connection.side_effect = lambda *a, **kw : raise_(socket.timeout())

    ret = main()
    assert ret == ErrorCode.NO_CONNECT

@pytest.mark.integration
@pytest.mark.mail
def test_refusing_connection(fake_config, no_connection, args):
    fake_config.use_section(full_blown_config)
    args.set_args("qs mail --to Alice msg body")

    ret = main()
    assert ret == ErrorCode.NO_CONNECT

@pytest.mark.integration
@pytest.mark.mail
@pytest.mark.parametrize("exc", [
    smtplib.SMTPRecipientsRefused(["Alice"]), 
    smtplib.SMTPHeloError(1, "msg"),
    smtplib.SMTPSenderRefused(1, "msg", full_blown_config["sender"]),
    smtplib.SMTPDataError(1, "msg"),
    # TODO: Python 3.5: smtplib.SMTPNotSupportedError(),
])
def test_sending_error(exc, fake_config, establish_connection, args):
    fake_config.use_section(full_blown_config)
    args.set_args("qs mail --to Alice msg body")

    establish_connection.send_message.side_effect = lambda *a, **kw : raise_(exc)

    ret = main()
    assert ret == ErrorCode.SEND_ERROR

@pytest.mark.integration
@pytest.mark.mail
def test_login(fake_config, establish_connection, args):
    cr = copy.copy(minimal_config)
    cr["user"] = "user"
    cr["password"] = "some-password"

    fake_config.use_section(cr)
    args.set_args("qs mail message --to Alice --sender username")

    ret = main()
    assert ret == ErrorCode.NO_ERROR
    establish_connection.login.assert_called_once_with(cr["user"], cr["password"])
    
@pytest.mark.integration
@pytest.mark.mail
def test_no_login_without_user(fake_config, establish_connection, args):
    cr = copy.copy(minimal_config)
    cr["password"] = "some-password"

    fake_config.use_section(cr)
    args.set_args("qs mail message --to Alice --sender username")

    ret = main()
    assert ret == ErrorCode.NO_ERROR
    assert 0 == establish_connection.login.call_count

@pytest.mark.integration
@pytest.mark.mail
@pytest.mark.parametrize("ask_pass,password", [
    (True, "normal-password"), (False, "normal-password"), (True, None), (False, None), ])
def test_ask_pass(ask_pass, password, fake_config, establish_connection, args, monkeypatch):
    cr = copy.copy(minimal_config)
    cr["sender"] = "user"
    cr["user"] = "username"
    cr["password"] = password
    cr["ask_pass"] = ask_pass

    fake_config.use_section(cr)
    args.set_args("qs mail message --to Alice")
    monkeypatch.setattr("getpass.getpass", lambda *a, **kw: "password-from-getpass")

    ret = main()
    assert ret == ErrorCode.NO_ERROR

    a, kw = establish_connection.login.call_args

    if password is None:
        if ask_pass is True:
            assert "password-from-getpass" in (a[1], kw.get("password"))
        else:
            assert "" in (a[1], kw.get("password"))
    else:
        assert password in (a[1], kw.get("password"))


@pytest.mark.integration
@pytest.mark.mail
@pytest.mark.parametrize("security", ["starttls", "StartTls", "STARTTLS"])
def test_starttls(security, fake_config, establish_connection, args):
    cr = copy.copy(minimal_config)
    cr["sender"] = "user"
    cr["security"] = security

    fake_config.use_section(cr)
    args.set_args("qs mail message --to Alice")

    ret = main()
    assert ret == ErrorCode.NO_ERROR
    establish_connection.starttls.assert_called_once_with()

@pytest.mark.integration
@pytest.mark.mail
@pytest.mark.parametrize("security", ["abc", "tls", "TLS"])
def test_starttls(security, fake_config, establish_connection, args):
    cr = copy.copy(minimal_config)
    cr["sender"] = "user"
    cr["security"] = security

    fake_config.use_section(cr)
    args.set_args("qs mail message --to Alice")

    ret = main()
    assert 0 == establish_connection.starttls.call_count

@pytest.mark.integration
@pytest.mark.mail
@pytest.mark.parametrize("security", ["ssl", "SSL", "Ssl"])
def test_ssl(security, fake_config, establish_connection_ssl, args):
    cr = copy.copy(minimal_config)
    cr["sender"] = "user"
    cr["security"] = security

    fake_config.use_section(cr)
    args.set_args("qs mail message --to Alice")

    ret = main()

    assert ret == ErrorCode.NO_ERROR
    establish_connection_ssl.send_message.assert_called_once_with(mock.ANY)

@pytest.mark.integration
@pytest.mark.mail
def test_sender_address(fake_config, establish_connection, args):
    fake_config.use_section(full_blown_config)
    args.set_args("qs mail msg body --to Alice")

    ret = main()
    assert ret == ErrorCode.NO_ERROR
    establish_connection.send_message.assert_called_once_with(mock.ANY)

    msg = get_sent_message(establish_connection)
    assert msg["From"] == full_blown_config["sender"]

@pytest.mark.integration
@pytest.mark.mail
@pytest.mark.parametrize("filename", ["attachment.txt", "My file.txt",
                                      "Josh's bills.pdf", 'Paula\'s "photos".'])
def test_attachment(fake_config, establish_connection, args, tmpdir, filename):
    f = tmpdir.join(filename)
    f.write("")

    fake_config.use_section(full_blown_config)
    args.set_args(['qs', 'mail', 'msg body', '--to', 'Alice', '-a', str(f)])

    ret = main()
    assert ret == ErrorCode.NO_ERROR
    establish_connection.send_message.assert_called_once_with(mock.ANY)

    msg = get_sent_message(establish_connection)

    attachment_present = False
    text_present = False

    assert msg.is_multipart() is True
    for p in msg.walk():
        if type(p) is email.mime.application.MIMEApplication:
            assert attachment_present is False
            attachment_present = True

            fname = os.path.basename(str(f)).replace('"', r'\"')
            assert "attachment" in p["Content-Disposition"]
            assert 'filename="%s"' % fname in p["Content-Disposition"]
            assert os.path.basename(str(f)) == p["Name"]
        if type(p) is email.mime.text.MIMEText:
            assert text_present is False
            text_present = True

            assert p.get_payload() == "msg body"

    assert text_present is True
    assert attachment_present is True

@pytest.mark.integration
@pytest.mark.mail
@pytest.mark.parametrize("kw", [
    { "sender": None },
    {"security": "trolololo"}, { "security": 123 },
    { "host": None },
    { "port": "123eej" },
    { "ask_pass" : "abcd" },
    { "contacts" : "wow" },
])
def test_incorrect_parameters(kw, fake_config, establish_connection, args):
    kw.setdefault("sender", "sth@example.com")
    kw.setdefault("host", "example.com")

    print(kw)

    fake_config.use_section(qsp.mail("example", **kw))
    args.set_args("qs mail msg body --to Alice")

    ret = main()
    assert ret == ErrorCode.BAD_INPUT
    assert establish_connection.send_message.called is False

@pytest.mark.integration
@pytest.mark.mail
@pytest.mark.parametrize("configuration", [full_blown_config, minimal_config, None])
def test_sending_sending_parameters_given_on_cli(configuration, fake_config, establish_connection_ssl, args):
    if configuration is not None:  # test empty (or non-existing) configuration file
        fake_config.use_section(configuration)

    args.set_args("qs mail \
        --host some.host \
        --port 527 \
        --security ssl \
        --user some-user \
        --password mypass \
        --sender jim@example.com \
        --to alice@corp.com \
        msg body")

    ret = main()
    assert ret == ErrorCode.NO_ERROR

    a, kw = establish_connection_ssl.call_args
    assert "some.host" in (a[0], kw.get("host"))
    assert 527 in (a[1], kw.get("port"))

    establish_connection_ssl.login.assert_called_once_with("some-user", "mypass")

    msg = get_sent_message(establish_connection_ssl)
    assert msg["From"] == "jim@example.com"

@pytest.mark.integration
@pytest.mark.mail
def test_sending_fail_on_missing_conf_even_when_all_params_are_given(fake_config, establish_connection_ssl, args):
    fake_config.use_section(full_blown_config)

    args.set_args("qs mail \
        -c some-unknown-configuration-name \
        --host some.host \
        --port 527 \
        --security ssl \
        --user some-user \
        --password mypass \
        --sender jim@example.com \
        --to alice@corp.com \
        msg body")

    ret = main()
    assert ret == ErrorCode.BAD_INPUT

@pytest.mark.integration
@pytest.mark.xmpp
def test_sending_override_only_some_cli_parameters(fake_config, establish_connection, args):
    fake_config.use_section(full_blown_config)

    args.set_args("qs mail \
        -c full_blown_config \
        --host some.host \
        --user some-user \
        --sender jim@example.com \
        --to alice@corp.com \
        msg body")

    ret = main()
    assert ret == ErrorCode.NO_ERROR

    a, kw = establish_connection.call_args
    assert "some.host" in (a[0], kw.get("host"))
    assert full_blown_config.port in (a[1], kw.get("port"))

    establish_connection.login.assert_called_once_with("some-user", full_blown_config.password)

    msg = get_sent_message(establish_connection)
    assert msg["From"] == "jim@example.com"
