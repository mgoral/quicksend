#-*- coding: utf-8 -*- #
# Copyright (C) 2015 Michał Góral.
#
# This file is part of quicksend
#
# quicksend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# quicksend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with quicksend. If not, see <http://www.gnu.org/licenses/>.

import copy
import pytest
import unittest.mock as mock

import qs.config

@pytest.fixture
def fake_stdin(monkeypatch):
    class FakeStdin:
        def __init__(self):
            self.in_lines = []

        def readlines(self):
            return self.in_lines

    ret = FakeStdin()
    monkeypatch.setattr("sys.stdin", ret)
    return ret

@pytest.fixture
def fake_getpass(monkeypatch):
    getpass_mock = mock.MagicMock()
    monkeypatch.setattr("getpass.getpass", lambda *args, **kwargs: getpass_mock)
    return getpass_mock
    
@pytest.fixture
def fake_config(tmpdir, monkeypatch, fake_getpass):
    class ConfigConfigurator:
        def __init__(self):
            def fake_read_config(_):
                return self._cr

            self._cr = {}
            monkeypatch.setattr(qs.config, "_read_config", fake_read_config)

        def use_section(self, cr):
            self._cr[cr._section_name] = copy.copy(cr)

        @property
        def config(self):
            return self._cr

    return ConfigConfigurator()

@pytest.fixture
def args(monkeypatch):
    class Args:
        def set_args(self, a):
            try:
                args = a.split()  # accept string
            except AttributeError:
                args = a          # accept list
            monkeypatch.setattr("sys.argv", args)
    return Args()

