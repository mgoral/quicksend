#-*- coding: utf-8 -*- #
# Copyright (C) 2015 Michał Góral.
#
# This file is part of quicksend
#
# quicksend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# quicksend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with quicksend. If not, see <http://www.gnu.org/licenses/>.

import pytest

from voluptuous import Schema, Invalid

from qsconf.protocols import mail, xmpp

def _get_schema(field):
    return field.__get__(None).validator

def _accepts_value_impl(proto_field, in_val, expected, msg = None):
    schema = _get_schema(proto_field)

    try:
        assert expected == schema(in_val)
    except Invalid:
        if msg is None:
            pytest.fail()
        else:
            pytest.fail(msg % dict(value=in_val, expected=expected, field=proto_field.name))

def accepts_value(proto_field, value, msg = None):
    _accepts_value_impl(proto_field, value, value,
        "%(value)s not accepted for field '%(field)s'")

def not_accepts_value(proto_field, value):
    schema = _get_schema(proto_field)
    with pytest.raises(Invalid):
        schema(value)

def mutates_value(proto_field, value, expected):
    if value == expected:
        raise ValueError("TEST BUG: value and expected are the same")
    _accepts_value_impl(proto_field, value, expected,
        "%(value)s not mutated into '%(expected)s'")

def is_required(proto_field):
    schema = _get_schema(proto_field)
    with pytest.raises(Invalid):
        schema(None)

def is_not_required(proto_field):
    schema = _get_schema(proto_field)

    try:
        out = schema(None)
        assert out is None
    except Invalid:
        pytest.fail("'%s' is required" % key)

def has_default(cls, attr, default):
    obj = cls("")
    assert getattr(obj, attr) == default

#############################################################################################
## XMPP
#############################################################################################

def test_xmpp_protocol():
    assert xmpp("").protocol == "xmpp"

def test_xmpp_jid():
    is_required(xmpp.jid)
    accepts_value(xmpp.jid, "abc")
    accepts_value(xmpp.jid, "ABC")
    accepts_value(xmpp.jid, "AbC")
    accepts_value(xmpp.jid, "123")
    mutates_value(xmpp.jid, 123, "123")

def test_xmpp_password():
    is_not_required(xmpp.password)
    accepts_value(xmpp.password, "123")
    mutates_value(xmpp.password, 123, "123")

def test_xmpp_host():
    accepts_value(xmpp.host, "abc.pl")
    accepts_value(xmpp.host, "Abc.pl")
    accepts_value(xmpp.host, "Abc.pl")
    accepts_value(xmpp.host, "ABC.PL")

def test_xmpp_port():
    accepts_value(xmpp.port, 123)
    mutates_value(xmpp.port, "123", 123)
    not_accepts_value(xmpp.port, "abc")

def test_xmpp_security():
    accepts_value(xmpp.security, "ssl")
    mutates_value(xmpp.security, "Ssl", "ssl")
    mutates_value(xmpp.security, "SSL", "ssl")

    accepts_value(xmpp.security, "tls")
    mutates_value(xmpp.security, "tLs", "tls")
    mutates_value(xmpp.security, "TLS", "tls")

    not_accepts_value(xmpp.security, "abc")
    not_accepts_value(xmpp.security, "ssll")
    not_accepts_value(xmpp.security, "tlss")
    not_accepts_value(xmpp.security, "tls ")
    not_accepts_value(xmpp.security, " ssl")
    not_accepts_value(xmpp.security, "starttls")

def test_xmpp_ask_pass():
    has_default(xmpp, "ask_pass", True)

    accepts_value(xmpp.ask_pass, True)
    accepts_value(xmpp.ask_pass, False)

    mutates_value(xmpp.ask_pass, "true", True)
    mutates_value(xmpp.ask_pass, "True", True)
    mutates_value(xmpp.ask_pass, "TRUE", True)

    mutates_value(xmpp.ask_pass, "false", False)
    mutates_value(xmpp.ask_pass, "False", False)
    mutates_value(xmpp.ask_pass, "FALSE", False)

def test_xmpp_contacts():
    accepts_value(xmpp.contacts, {})
    accepts_value(xmpp.contacts, {"abc": "def", "ihj": "klm"})
    not_accepts_value(xmpp.contacts, {"abc": 123})

#############################################################################################
## MAIL
#############################################################################################

def test_mail_protocol():
    assert mail("").protocol == "mail"

def test_mail_from():
    is_required(mail.sender)
    accepts_value(mail.sender, "abc")
    accepts_value(mail.sender, "aBc")
    accepts_value(mail.sender, "ABC")
    accepts_value(mail.sender, "123")
    mutates_value(mail.sender, 123, "123")

def test_mail_user():
    is_not_required(mail.user)
    accepts_value(mail.user, "abc")
    accepts_value(mail.user, "aBc")
    accepts_value(mail.user, "ABC")
    accepts_value(mail.user, "123")
    mutates_value(mail.user, 123, "123")

def test_mail_password():
    is_not_required(mail.password)
    accepts_value(mail.password, "abc")
    accepts_value(mail.password, "aBc")
    accepts_value(mail.password, "ABC")
    accepts_value(mail.password, "123")
    mutates_value(mail.password, 123, "123")

def test_mail_host():
    is_required(mail.host)
    accepts_value(mail.host, "abc.pl")
    accepts_value(mail.host, "aBc.pL")
    accepts_value(mail.host, "ABC.PL")
    accepts_value(mail.host, "123")
    mutates_value(mail.host, 123, "123")

def test_mail_port():
    has_default(mail, "port", 25)

    accepts_value(mail.port, 123)
    mutates_value(mail.port, "123", 123)
    not_accepts_value(mail.port, "abc")

def test_mail_security():
    accepts_value(mail.security, "ssl")
    mutates_value(mail.security, "Ssl", "ssl")
    mutates_value(mail.security, "SSL", "ssl")

    accepts_value(mail.security, "starttls")
    mutates_value(mail.security, "stArtTls", "starttls")
    mutates_value(mail.security, "STARTTLS", "starttls")

    not_accepts_value(mail.security, "abc")
    not_accepts_value(mail.security, "tls")
    not_accepts_value(mail.security, "ssll")
    not_accepts_value(mail.security, "tlss")
    not_accepts_value(mail.security, "starttls ")
    not_accepts_value(mail.security, " starttls")
    not_accepts_value(mail.security, " ssl")

def test_mail_ask_pass():
    has_default(mail, "ask_pass", True)

    accepts_value(mail.ask_pass, True)
    accepts_value(mail.ask_pass, False)

    mutates_value(mail.ask_pass, "true", True)
    mutates_value(mail.ask_pass, "True", True)
    mutates_value(mail.ask_pass, "TRUE", True)

    mutates_value(mail.ask_pass, "false", False)
    mutates_value(mail.ask_pass, "False", False)
    mutates_value(mail.ask_pass, "FALSE", False)

def test_mail_contacts():
    accepts_value(mail.contacts, {})
    accepts_value(mail.contacts, {"abc": "def", "ihj": "klm"})
    not_accepts_value(mail.contacts, {"abc": 123})
