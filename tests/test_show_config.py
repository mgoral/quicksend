# Copyright (C) 2015 Michał Góral.
#
# This file is part of quicksend
#
# quicksend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# quicksend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with quicksend. If not, see <http://www.gnu.org/licenses/>.

import os
import pytest

from qs.apprunner import main
from qs.detail.codes import ErrorCode
import qsconf.protocols as qsp

xmpp_config = qsp.xmpp("test-xmpp")
xmpp_config.jid = "jid@example.com"
xmpp_config.port = 5511
xmpp_config.password = "Super-Strong"
xmpp_config.contacts = { "Daniel" : "daniel@example.com" }

mail_config = qsp.mail("test-mail")
mail_config.sender = "user@example.com"
mail_config.host = "localhost"
mail_config.port = 5511
mail_config.user = "UserName"
mail_config.password = "Mail-PassworD"

def find_lines(search, iterable):
    return [item for item in iterable if search in item]

@pytest.fixture
def nolang(monkeypatch):
    # just remove all variables which might get in our way
    # https://www.gnu.org/savannah-checkouts/gnu/libc/manual/html_node/Locale-Categories.html
    monkeypatch.delenv("LANG", raising=False)
    monkeypatch.delenv("LANGUAGE", raising=False)  # libc's extension
    monkeypatch.delenv("LC_ALL", raising=False)
    monkeypatch.delenv("LC_MESSAGES", raising=False)
    monkeypatch.delenv("LC_COLLATE", raising=False)
    monkeypatch.delenv("LC_CTYPE", raising=False)
    monkeypatch.delenv("LC_MONETARY", raising=False)
    monkeypatch.delenv("LC_NUMERIC", raising=False)
    monkeypatch.delenv("LC_TIME", raising=False)


@pytest.mark.parametrize("protocol_switch", ["", "--with-protocol"])
def test_printing_sections(protocol_switch, capsys, args, fake_config, nolang):
    fake_config.use_section(xmpp_config)
    fake_config.use_section(mail_config)

    args.set_args("qs show-config %s" % protocol_switch)

    ErrorCode.NO_ERROR = main()

    out, _ = capsys.readouterr()
    out = out.strip().split(os.linesep)

    assert len(out) == 2
    if protocol_switch == "":
        assert xmpp_config.name in out
        assert mail_config.name in out
    else:
        for cfg in [xmpp_config, mail_config]:
            lines = find_lines(cfg.name, out)
            assert len(lines) == 1
            assert cfg.name in lines[0]
            assert cfg.protocol in lines[0]


def test_printing_config_bad_name(capsys, args, fake_config, nolang):
    fake_config.use_section(xmpp_config)
    fake_config.use_section(mail_config)

    args.set_args("qs show-config somethingsomething")
    assert ErrorCode.BAD_INPUT == main()


@pytest.mark.parametrize("switches", ["",
                                      "--with-protocol",
                                      "--with-password",
                                      "--with-protocol --with-password"])
def test_printing_config(switches, capsys, args, fake_config, nolang):
    fake_config.use_section(xmpp_config)
    fake_config.use_section(mail_config)

    args.set_args("qs show-config %s %s" % (mail_config.name, switches))

    ErrorCode.NO_ERROR = main()

    out, _ = capsys.readouterr()
    out = out.strip().split(os.linesep)

    for attr in mail_config.attributes():
        lines = find_lines("%s:" % attr, out)
        if getattr(mail_config, attr) is not None:  # if this particular attribute is set
            assert len(lines) == 1
            if attr != "password":
                assert str(getattr(mail_config, attr)) in lines[0]

    # special cases:
    proto_lines = find_lines("protocol:", out)
    pass_lines = find_lines("password:", out)

    if "--with-protocol" in switches:
        assert len(proto_lines) == 1
        assert mail_config.protocol in proto_lines[0]
    else:
        assert len(proto_lines) == 0

    if "--with-password" in switches:
        assert mail_config.password in pass_lines[0]
    else:
        assert mail_config.password not in pass_lines[0]

