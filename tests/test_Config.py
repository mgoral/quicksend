#-*- coding: utf-8 -*- #
# Copyright (C) 2015 Michał Góral.
#
# This file is part of quicksend
#
# quicksend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# quicksend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with quicksend. If not, see <http://www.gnu.org/licenses/>.

import pytest
import os
import tempfile

from qs.config import Config, choose_section, get_contacts
from qs.config import field_matches, is_protocol, is_protocol_with_field
from qs.config import _get_config_search_paths, _find_config
from qs.detail.codes import ExceptionWithCode

import qsconf.protocols as qsp
from qsconf._protocol_base import ProtocolConfiguration

home = os.path.expanduser("~")

class EmptyProto(ProtocolConfiguration):
    jid = None

@pytest.fixture
def dummy_config(fake_config):
    fake_config.use_section(qsp.xmpp("example.com", jid = "user@example.com"))
    fake_config.use_section(qsp.mail("dummy.com", user = "user@dummy.com"))
    fake_config.use_section(qsp.xmpp("server.com", jid = "user@server.com"))
    fake_config.use_section(EmptyProto("strange.com", None, jid = "user@strange.com"))
    return fake_config

def test_return_empty_values_when_there_is_no_config(fake_config):
    c = Config()
    assert c.get_section("example.com") is None

def test_return_only_correct_config_informations(dummy_config):
    c = Config()

    # incorrect names
    assert c.get_section("unknown-server") is None

    # default values
    default = { "key1" : "value1", "key2" : "value2" }
    assert "value1" == c.get_section("unknown-server", default).get("key1")
    assert "value2" == c.get_section("unknown-server", default).get("key2")

    # validate data
    assert "user@example.com" == c.get_section("example.com").get("jid")
    assert "xmpp" == c.get_section("example.com").get("protocol")

    assert "user@dummy.com" == c.get_section("dummy.com").get("user")
    assert "mail" == c.get_section("dummy.com").get("protocol")

    assert "user@server.com" == c.get_section("server.com").get("jid")
    assert "xmpp" == c.get_section("server.com").get("protocol")

    assert "user@strange.com" == c.get_section("strange.com").get("jid")

    assert c.has_section("example.com") is True
    assert c.has_section("eXaMpLe.cOm") is False
    assert c.has_section("unknown.com") is False

def test_finding_sections(dummy_config):
    c = Config()

    def all_sections(section): return True
    def no_sections(section): return False
    def all_xmpp(section): return section.get("protocol") == "xmpp"

    assert list(dummy_config.config.keys()) == c.find_sections(all_sections)
    assert len(c.find_sections(no_sections)) == 0

    xmpp_sections = c.find_sections(all_xmpp)
    assert 2 == len(xmpp_sections)
    assert ("example.com" in xmpp_sections) is True
    assert ("server.com" in xmpp_sections) is True

def test_field_matches(dummy_config):
    section = { "key": "value" }
    empty_section = {}

    assert field_matches(section, "key", "value") is True
    assert field_matches(section, "abcd", "value") is False
    assert field_matches(section, "key", "foobar") is False
    assert field_matches(section, None, "value") is False
    assert field_matches(section, "key", None) is False
    assert field_matches(section, None, None) is False

    # check empty section
    assert field_matches(empty_section, "key", "value") is False

def test_is_protocol(dummy_config):
    section = { "protocol": "xmpp" }
    empty_section = {}

    assert is_protocol(section, "xmpp") is True
    assert is_protocol(section, "other") is False
    assert is_protocol(section, None) is False

    # check empty section
    assert is_protocol(empty_section, "xmpp") is False

def test_is_protocol_with_field_predicate(dummy_config):
    section = { "protocol": "dummy", "key": "value" }
    empty_section = {}

    # Basically we test all permutations of "strange" arguments
    assert is_protocol_with_field(section, "dummy", "key", "value") is True
    assert is_protocol_with_field(section, "unknown", "key", "value") is False
    assert is_protocol_with_field(section, "dummy", "unknown", "value") is False
    assert is_protocol_with_field(section, "dummy", "key", "unknown") is False
    assert is_protocol_with_field(section, "unknown", "unknown", "value") is False
    assert is_protocol_with_field(section, "unknown", "key", "unknown") is False
    assert is_protocol_with_field(section, "dummy", "unknown", "unknown") is False
    assert is_protocol_with_field(section, "unknown", "unknown", "unknown") is False
    assert is_protocol_with_field(section, None, "key", "value") is False
    assert is_protocol_with_field(section, "dummy", None, "value") is False
    assert is_protocol_with_field(section, "dummy", "key", None) is False
    assert is_protocol_with_field(section, None, None, "value") is False
    assert is_protocol_with_field(section, None, "key", None) is False
    assert is_protocol_with_field(section, "dummy", None, None) is False
    assert is_protocol_with_field(section, None, None, None) is False

    # check empty section
    assert is_protocol_with_field(empty_section, "dummy", "key", "value") is False

def test_choose_section(dummy_config):
    c = Config()

    # choosing by section
    assert ("server.com", dummy_config.config["server.com"]) == choose_section(c, "server.com", None, None, None)
    assert ("dummy.com", dummy_config.config["dummy.com"]) == choose_section(c, "dummy.com", "mail", "user", "user@dummy.com")
    assert ("dummy.com", dummy_config.config["dummy.com"]) == choose_section(c, "dummy.com", "xmpp", None, None)
    with pytest.raises(ExceptionWithCode):
        choose_section(c, "unknown", None, None, None)
    with pytest.raises(ExceptionWithCode):
        choose_section(c, None, None, None, None)

    # choosing by protocol and fields
    assert ("example.com", dummy_config.config["example.com"]) == choose_section(c, None, "xmpp", "jid", "user@example.com")
    with pytest.raises(ExceptionWithCode):
        choose_section(c, None, "xmpp", "jid", None)
    with pytest.raises(ExceptionWithCode):
        choose_section(c, None, "xmpp", None, "user@example.com")
    with pytest.raises(ExceptionWithCode):
        choose_section(c, None, None, "jid", "user@example.com")
    with pytest.raises(ExceptionWithCode):
        choose_section(c, None, None, "jid", None)
    with pytest.raises(ExceptionWithCode):
        choose_section(c, None, None, None, "user@example.com")

    # choosing by protocol
    assert ("dummy.com", dummy_config.config["dummy.com"]) == choose_section(c, None, "mail", None, None)
    assert ("dummy.com", dummy_config.config["dummy.com"]) == choose_section(c, None, "mail", "jid", None)
    assert ("dummy.com", dummy_config.config["dummy.com"]) == choose_section(c, None, "mail", "user", None)
    assert ("dummy.com", dummy_config.config["dummy.com"]) == choose_section(c, None, "mail", None, "user@example.com")
    with pytest.raises(ExceptionWithCode):
        choose_section(c, "xmpp", None, None, None)

def test_choose_section_empty_config(fake_config):
    c = Config()
    name, section = choose_section(c, None, "mail", None, None)
    assert name is None
    assert section.protocol == "mail"

def test_choose_section_ambiguous_fields(dummy_config):
    dummy_config.use_section(qsp.xmpp("first.com", jid = "user"))
    dummy_config.use_section(qsp.xmpp("second.com", jid = "user"))
    c = Config()
    with pytest.raises(ExceptionWithCode):
        choose_section(c, None, "xmpp", "jid", "user")

def test_validate_default_xdg_search_paths(monkeypatch):
    monkeypatch.delenv("XDG_CONFIG_HOME", raising = False)
    monkeypatch.delenv("XDG_CONFIG_DIRS", raising = False)

    expected = ["%s/.config" % home, "/etc/xdg"]
    assert expected == _get_config_search_paths()


def test_validate_searching_xdg_config_home(monkeypatch):
    monkeypatch.setenv("XDG_CONFIG_HOME", "/home/user/superconfig")
    monkeypatch.delenv("XDG_CONFIG_DIRS", raising = False)

    expected = [os.environ["XDG_CONFIG_HOME"], "/etc/xdg"]
    assert expected == _get_config_search_paths()

def test_validate_searching_xdg_config_dirs(monkeypatch):
    monkeypatch.delenv("XDG_CONFIG_HOME", raising = False)
    monkeypatch.setenv("XDG_CONFIG_DIRS", "/etc/superconfig:/usr/config:/home/user/configuration")

    expected = ["%s/.config" % home,
        "/etc/superconfig",
        "/usr/config",
        "/home/user/configuration"
    ]

    assert expected == _get_config_search_paths()

def test_validate_getting_config_file(tmpdir, monkeypatch):
    tempdir = tempfile.TemporaryDirectory()
    qsrc_path = tmpdir.mkdir("quicksend").join("qsrc.py") 
    qsrc_path.write("")

    monkeypatch.setenv("XDG_CONFIG_HOME", tmpdir)
    monkeypatch.setenv("XDG_CONFIG_DIRS",  "/impossible/path/trolololo")

    assert qsrc_path == _find_config()

def test_validate_not_finding_config_returns_None(monkeypatch):
    monkeypatch.setenv("XDG_CONFIG_HOME", "/impossible/path/trolololo")
    monkeypatch.setenv("XDG_CONFIG_DIRS", "/impossible/path/trolololo")

    assert _find_config() is None

def test_get_contacts():
    dummy_section = {"contacts" : { "alice" : "alice@example.com", "bob" : "bob@example.com" }}
    lookup = ("bob", "Bob", "bob", "alice")

    assert lookup == get_contacts(None, lookup)
    assert lookup == get_contacts(dict(), lookup)
    assert ["bob@example.com", "Bob", "bob@example.com", "alice@example.com"] == get_contacts(dummy_section, lookup)

