#-*- coding: utf-8 -*- #
# Copyright (C) 2015 Michał Góral.
#
# This file is part of quicksend
#
# quicksend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# quicksend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with quicksend. If not, see <http://www.gnu.org/licenses/>.

import itertools
import unittest.mock as mock

import pytest
import sleekxmpp

from qs.apprunner import main
import qs.config
import qs.senders.xmpp
from qs.detail.codes import ErrorCode

import qsconf.protocols as qsp

full_blown_config = qsp.xmpp("full_blown_config")
full_blown_config.jid = "another-jid@another-exampe.com"
full_blown_config.security = "sSL"
full_blown_config.host = "localhost"
full_blown_config.port = 5511
full_blown_config.password = "H4x00r$+yl3 p455\/\/-rD"
full_blown_config.ask_pass = "false"
full_blown_config.contacts = { "Daniel" : "daniel@example.com" }

minimal_config = qsp.xmpp("minimal_config")
minimal_config.jid = "jid@yet-another-exampe.com"

def store_handler(storage, name, handler):
    assert name not in storage
    storage[name] = handler

def call_handlers(storage, *args, raising = True):
    for name in args:
        handler = storage.get(name)
        if raising is False:
            if handler is not None:
                handler(mock.MagicMock())
        else:
            assert handler is not None
            handler(mock.MagicMock())

@pytest.fixture
def fake_connection(monkeypatch):
    class ConnectionManager:
        def __init__(self):
            self.mock = mock.create_autospec(sleekxmpp.ClientXMPP)
            self.use_proxy_mock = mock.PropertyMock()
            self.proxy_config_mock = mock.PropertyMock()
            type(self.mock).use_proxy = self.use_proxy_mock
            type(self.mock).proxy_config = self.proxy_config_mock

            self.mock.return_value = self.mock # trolololo, going solo! :]
            self.handlers = {}

    cm = ConnectionManager()
    monkeypatch.setattr("qs.senders.xmpp.sleekxmpp.ClientXMPP", cm.mock)
    return cm

@pytest.fixture
def establish_connection(fake_connection, request):
    fake_connection.mock.add_event_handler.side_effect = \
        lambda n, h: store_handler(fake_connection.handlers, n, h)
    fake_connection.mock.connect.side_effect = lambda *args, **kwargs : True
    fake_connection.mock.process = \
        lambda *args, **kwargs: call_handlers(fake_connection.handlers, "session_start")

    request.addfinalizer(
        lambda: call_handlers(fake_connection.handlers, "session_end", "disconnected", raising=False))

    return fake_connection

@pytest.fixture
def no_connection(fake_connection):
    fake_connection.mock.add_event_handler.side_effect = lambda n, h: store_handler(fake_connection.handlers, n, h)
    fake_connection.mock.connect.side_effect = lambda *args, **kwargs : False

    return fake_connection

@pytest.fixture
def no_auth(fake_connection, request):
    fake_connection.mock.add_event_handler.side_effect = \
        lambda n, h: store_handler(fake_connection.handlers, n, h)
    fake_connection.mock.connect.side_effect = \
        lambda *args, **kwargs : False
    fake_connection.mock.process = \
        lambda *args, **kwargs: call_handlers(fake_connection.handlers, "failed_auth")

    request.addfinalizer(lambda: call_handlers(fake_connection.handlers, "disconnected"))

    return fake_connection


#############################################################################################
## TESTS START HERE
#############################################################################################


@pytest.mark.integration
@pytest.mark.xmpp
@pytest.mark.parametrize("configuration", [full_blown_config, minimal_config])
def test_missing_recipient(configuration, fake_config, establish_connection, args):
    fake_config.use_section(configuration)
    args.set_args("qs xmpp message")

    with pytest.raises(SystemExit):
        main()

@pytest.mark.integration
@pytest.mark.xmpp
@pytest.mark.parametrize("configuration", [full_blown_config, minimal_config])
def test_message_from_stdin(configuration, fake_config, establish_connection, fake_stdin, args):
    fake_config.use_section(configuration)
    fake_stdin.in_lines = "Hello, this is stdin test!"
    args.set_args("qs xmpp --to Alice")

    ret = main()
    assert ret == ErrorCode.NO_ERROR
    assert establish_connection.mock.send_message.call_count == 1
    establish_connection.mock.send_message.assert_any_call(
        mto = "Alice", mbody = "Hello, this is stdin test!", mtype = mock.ANY)

@pytest.mark.integration
@pytest.mark.xmpp
@pytest.mark.parametrize("configuration", [full_blown_config, minimal_config])
def test_message_from_file(configuration, fake_config, establish_connection, tmpdir, args):
    fake_config.use_section(configuration)

    f = tmpdir.mkdir("fileinput_test").join("input.txt")
    f.write("Hello, this is the test of XMPP file input!")

    args.set_args("qs xmpp --to Alice -F %s" % f.realpath())

    ret = main()
    assert ret == ErrorCode.NO_ERROR
    assert establish_connection.mock.send_message.call_count == 1
    establish_connection.mock.send_message.assert_any_call(
        mto = "Alice", mbody = "Hello, this is the test of XMPP file input!", mtype = mock.ANY)

@pytest.mark.integration
@pytest.mark.xmpp
@pytest.mark.parametrize("configuration", [full_blown_config, minimal_config])
def test_sending_to_multiple_recipients(configuration, fake_config, establish_connection, args):
    fake_config.use_section(configuration)
    args.set_args("qs xmpp --to Alice msg body --to Charlie")

    ret = main()
    assert ret == ErrorCode.NO_ERROR

    # check if message has been sent
    assert establish_connection.mock.send_message.call_count == 2
    establish_connection.mock.send_message.assert_any_call(
        mto = "Alice", mbody = "msg body", mtype = mock.ANY)
    establish_connection.mock.send_message.assert_any_call(
        mto = "Charlie", mbody = "msg body", mtype = mock.ANY)

@pytest.mark.integration
@pytest.mark.xmpp
@pytest.mark.parametrize("configuration", [full_blown_config, minimal_config])
def test_no_auth(configuration, fake_config, no_auth, args):
    fake_config.use_section(configuration)
    args.set_args("qs xmpp --to Alice msg body")

    ret = main()
    assert ret == ErrorCode.NO_CONNECT
    assert no_auth.mock.send_message.called is False

@pytest.mark.integration
@pytest.mark.xmpp
@pytest.mark.parametrize("configuration", [full_blown_config, minimal_config])
def test_disconnect(configuration, fake_config, no_connection, args):
    fake_config.use_section(configuration)
    args.set_args("qs xmpp --to Alice msg body")

    no_connection.mock.process = \
        lambda *args, **kwargs: call_handlers(no_connection.handlers, "disconnected")

    ret = main()
    assert ret == ErrorCode.NO_CONNECT
    assert no_connection.mock.send_message.called is False

@pytest.mark.integration
@pytest.mark.xmpp
@pytest.mark.parametrize("configuration", [full_blown_config, minimal_config])
def test_missing_configuration(configuration, fake_config, establish_connection, args):
    fake_config.use_section(configuration)
    args.set_args("qs xmpp -c doesnt-exist --to Alice msg")

    ret = main()
    assert ret == ErrorCode.BAD_INPUT
    assert establish_connection.mock.send_message.called is False

@pytest.mark.integration
@pytest.mark.xmpp
@pytest.mark.parametrize("kw", [
    { "jid": None },
    { "security": "starttls" }, {"security": "trolololo"}, { "security": 123 },
    { "port": "123eej" },
    { "ask_pass" : "abcd" },
    { "contacts" : "wow" },
])
def test_incorrect_parameters(kw, fake_config, establish_connection, args):
    kw.setdefault("jid", "jid@example.com")

    fake_config.use_section(qsp.xmpp("example", **kw))

    args.set_args("qs xmpp --to Alice msg")
    ret = main()

    assert ret == ErrorCode.BAD_INPUT
    assert establish_connection.mock.send_message.called is False

@pytest.mark.integration
@pytest.mark.xmpp
@pytest.mark.parametrize("param, exp_use_ssl, exp_use_tls", [
    ("ssl", True, False), ("sSl", True, False), ("SSL", True, False),
    ("tls", False, True), ("tlS", False, True), ("TLS", False, True),
    (None, False, False),
])
def test_security(param, exp_use_ssl, exp_use_tls, fake_config, establish_connection, args):
    fake_config.use_section(qsp.xmpp("example", jid = "jid@example.com", security = param))
    args.set_args("qs xmpp --to Alice msg")

    ret = main()
    assert ret == ErrorCode.NO_ERROR
    establish_connection.mock.connect.assert_called_once_with(
        use_ssl = exp_use_ssl, use_tls = exp_use_tls, reattempt = mock.ANY)

@pytest.mark.integration
@pytest.mark.xmpp
@pytest.mark.parametrize("host, port", [("abc", 123), ("def", "23221")])
def test_address(host, port, fake_config, establish_connection, args):
    fake_config.use_section(qsp.xmpp("example", jid = "jid@example.com", host = host, port = port))
    args.set_args("qs xmpp --to Alice msg")

    ret = main()
    assert ret == ErrorCode.NO_ERROR
    establish_connection.mock.connect.assert_called_once_with(
        address = (host, int(port)), reattempt = mock.ANY, use_ssl = mock.ANY, use_tls = mock.ANY)

@pytest.mark.integration
@pytest.mark.xmpp
@pytest.mark.parametrize("host, port", [("abc", None), (None, 23221)])
def test_address_no_use(host, port, fake_config, establish_connection, args):
    fake_config.use_section(qsp.xmpp("example", jid = "jid@example.com", host = host, port = port))
    args.set_args("qs xmpp --to Alice msg")

    ret = main()
    assert ret == ErrorCode.NO_ERROR

@pytest.mark.integration
@pytest.mark.xmpp
@pytest.mark.parametrize("configuration", [full_blown_config, minimal_config])
def test_jid_override(configuration, fake_config, establish_connection, args):
    fake_config.use_section(configuration)
    args.set_args("qs xmpp --jid other-jid@example2.com.pl message --to Alice")

    ret = main()
    assert ret == ErrorCode.NO_ERROR

    a, kw = establish_connection.mock.call_args
    assert "other-jid@example2.com.pl" in (a[0], kw.get("jid"))

@pytest.mark.integration
@pytest.mark.xmpp
@pytest.mark.parametrize("ask_pass,password", [
    (True, "normal-password"), (False, "normal-password"), (True, None), (False, None), ])
def test_ask_pass(ask_pass, password, fake_config, establish_connection, args, monkeypatch):
    fake_config.use_section(
        qsp.xmpp("example", jid = "jid@example.com", ask_pass = ask_pass, password = password))
    args.set_args("qs xmpp message --to Alice")
    monkeypatch.setattr("getpass.getpass", lambda *a, **kw: "password-from-getpass")

    ret = main()
    assert ret == ErrorCode.NO_ERROR

    a, kw = establish_connection.mock.call_args

    if password is None:
        if ask_pass is True:
            assert "password-from-getpass" in (a[1], kw.get("password"))
        else:
            assert "" in (a[1], kw.get("password"))
    else:
        assert password in (a[1], kw.get("password"))

@pytest.mark.integration
@pytest.mark.xmpp
def test_sending_to_contact(fake_config, establish_connection, args):
    fake_config.use_section(full_blown_config)
    args.set_args("qs xmpp --to Alice --to Daniel --to Charlie msg body")

    ret = main()
    assert ret == ErrorCode.NO_ERROR

    # check if message has been sent
    assert establish_connection.mock.send_message.call_count == 3
    establish_connection.mock.send_message.assert_any_call(
        mto = "Alice", mbody = "msg body", mtype = mock.ANY)
    establish_connection.mock.send_message.assert_any_call(
        mto = full_blown_config["contacts"]["Daniel"], mbody = "msg body", mtype = mock.ANY)
    establish_connection.mock.send_message.assert_any_call(
        mto = "Charlie", mbody = "msg body", mtype = mock.ANY)

@pytest.mark.integration
@pytest.mark.xmpp
def test_sending_missing_parameters(fake_config, establish_connection, args):
    args.set_args("qs xmpp --to Alice msg body")

    ret = main()
    assert ret == ErrorCode.BAD_INPUT

@pytest.mark.integration
@pytest.mark.xmpp
@pytest.mark.parametrize("configuration", [full_blown_config, minimal_config, None])
def test_sending_sending_parameters_given_on_cli(configuration, fake_config, establish_connection, args):
    if configuration is not None:  # test empty (or non-existing) configuration file
        fake_config.use_section(configuration)

    args.set_args("qs xmpp \
        --host some.host \
        --port 7756 \
        --security tls \
        --jid myjid \
        --password mypass \
        --to Alice \
        msg body")

    ret = main()

    assert ret == ErrorCode.NO_ERROR

    establish_connection.mock.send_message.assert_called_once_with(
        mto = "Alice", mbody = "msg body", mtype = mock.ANY)

    establish_connection.mock.connect.assert_called_once_with(
        address = ("some.host", 7756), use_ssl = False, use_tls = True, reattempt = mock.ANY)

    a, kw = establish_connection.mock.call_args
    assert "myjid" in (a[0], kw.get("jid"))
    assert "mypass" in (a[1], kw.get("password"))

@pytest.mark.integration
@pytest.mark.xmpp
def test_sending_fail_on_missing_conf_even_when_all_params_are_given(fake_config, establish_connection, args):
    fake_config.use_section(full_blown_config)

    args.set_args("qs xmpp \
        -c some-unknown-configuration-name \
        --host some.host \
        --port 7756 \
        --security tls \
        --jid myjid \
        --password mypass \
        --to Alice \
        msg body")

    ret = main()
    assert ret == ErrorCode.BAD_INPUT

@pytest.mark.integration
@pytest.mark.xmpp
def test_sending_override_only_some_cli_parameters(fake_config, establish_connection, args):
    fake_config.use_section(full_blown_config)

    args.set_args("qs xmpp \
        -c full_blown_config \
        --host some.host \
        --jid myjid \
        --pass mypass \
        --to Alice \
        msg body")

    ret = main()
    assert ret == ErrorCode.NO_ERROR

    establish_connection.mock.send_message.assert_called_once_with(
        mto = "Alice", mbody = "msg body", mtype = mock.ANY)

    establish_connection.mock.connect.assert_called_once_with(
        address = ("some.host", full_blown_config.port),
        use_ssl = True, use_tls = False, reattempt = mock.ANY)

    a, kw = establish_connection.mock.call_args
    assert "myjid" in (a[0], kw.get("jid"))
    assert "mypass" in (a[1], kw.get("password"))

@pytest.mark.integration
@pytest.mark.xmpp
@pytest.mark.parametrize("params",
                         list(itertools.product(('--jid myjid --host some.host', '--jid myjid'),
                                                ('http_proxy', 'https_proxy'),
                                                (None, 'localhost,127.0.0.1,example.com'))))
def test_proxy(fake_config, establish_connection, args, monkeypatch, params):
    additional_args, proxy_type, no_proxy = params
    monkeypatch.setenv(proxy_type, 'http://user:pass@10.144.2.3:8080')
    if no_proxy:
        monkeypatch.setenv('no_proxy', no_proxy)
    args.set_args("qs xmpp %s --to Alice msg body" % additional_args)

    ret = main()
    assert ret == ErrorCode.NO_ERROR

    # These are checked differently because of different access methods.
    # MagicMock gives us __setitem__ (how proxy_config is set), but doesn't give
    # __setattr__ (how senders.XMPP._client.use_proxy is set).
    establish_connection.use_proxy_mock.assert_called_once_with(True)
    establish_connection.mock.proxy_config.__setitem__.assert_any_call(
        'host', '10.144.2.3')
    establish_connection.mock.proxy_config.__setitem__.assert_any_call(
        'port', 8080)
    establish_connection.mock.proxy_config.__setitem__.assert_any_call(
        'username', 'user')
    establish_connection.mock.proxy_config.__setitem__.assert_any_call(
        'password', 'pass')

@pytest.mark.integration
@pytest.mark.xmpp
@pytest.mark.parametrize("params",
                         list(itertools.product(('--jid jid@something --host xmpp.myhost.com',
                                                 '--jid jid@xmpp.myhost.com'),
                                                ('http_proxy', 'https_proxy'),
                                                ('localhost,127.0.0.1,myhost.com',
                                                  'localhost,127.0.0.1,*myhost.com',
                                                  'localhost,127.0.0.1,*.myhost.com',
                                                  '*'))))
def test_no_proxy(fake_config, establish_connection, args, monkeypatch, params):
    additional_args, proxy_type, no_proxy = params
    monkeypatch.setenv(proxy_type, 'http://user:pass@10.144.2.3:8080')
    monkeypatch.setenv('no_proxy', no_proxy)
    args.set_args("qs xmpp %s --to Alice msg body" % additional_args)

    ret = main()
    assert ret == ErrorCode.NO_ERROR
    assert 1 == establish_connection.mock.connect.call_count
    assert 0 == establish_connection.use_proxy_mock.call_count
    assert 0 == establish_connection.mock.proxy_config.__setitem__.call_count

@pytest.mark.integration
@pytest.mark.xmpp
def test_no_proxy_set(fake_config, establish_connection, args):
    fake_config.use_section(full_blown_config)

    args.set_args("qs xmpp \
        -c full_blown_config \
        --host some.host \
        --jid myjid \
        --pass mypass \
        --to Alice \
        msg body")

    ret = main()
    assert ret == ErrorCode.NO_ERROR
    assert 0 == establish_connection.use_proxy_mock.call_count
    assert 0 == establish_connection.mock.proxy_config.__setitem__.call_count
