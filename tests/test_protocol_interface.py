#-*- coding: utf-8 -*- #
# Copyright (C) 2015 Michał Góral.
#
# This file is part of quicksend
#
# quicksend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# quicksend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with quicksend. If not, see <http://www.gnu.org/licenses/>.

import pytest

from qs.commands._interface import ProtocolInterface
from qs.detail.codes import ExceptionWithCode

@pytest.fixture
def fake_args():
    class Args:
        message = None
        msg_file = None
    return Args()

@pytest.fixture
def sut():
    return ProtocolInterface("name", "description")

def test_read_message(sut, fake_args):
    fake_args.message = ["This", "was", "a", "triumph!"]
    msg = sut._read_message(fake_args)
    assert msg == "This was a triumph!"

def test_read_message_from_file(sut, tmpdir, fake_args):
    f = tmpdir.join("test_file.txt")
    f.write("a\nb\n\nc\n")

    fake_args.msg_file = str(f.realpath())

    msg = sut._read_message(fake_args)
    assert msg == "a\nb\n\nc\n"

def test_read_message_graceful_error_on_directory(sut, tmpdir, fake_args):
    d = tmpdir.mkdir("dir_name")
    fake_args.msg_file = str(d.realpath())

    with pytest.raises(ExceptionWithCode):
        sut._read_message(fake_args)

def test_read_message_graceful_error_on_incorrect_file(sut, tmpdir, fake_args):
    d = tmpdir.mkdir("dir_name")
    fake_args.msg_file = "%s/some_file.txt" % d.realpath()

    with pytest.raises(ExceptionWithCode):
        sut._read_message(fake_args)

def test_read_stdin(sut, fake_args, fake_stdin):
    fake_stdin.in_lines = "This was a triumph!"
    msg = sut._read_message(fake_args)
    assert msg == "This was a triumph!"
