#-*- coding: utf-8 -*- #
# Copyright (C) 2015 Michał Góral.
#
# This file is part of quicksend
#
# quicksend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# quicksend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with quicksend. If not, see <http://www.gnu.org/licenses/>.

import fnmatch
import itertools
import pytest

from qs.environ import Environment
from qs.detail.utils import expect, choose_proxy, find
from qs.detail.codes import ExceptionWithCode

def make_params():
    http_proxies = [None, 'http://1.1.1.1:8080']
    https_proxies = [None, 'http://2.2.2.2:8181']
    no_proxies = [None, 'localhost,127.0.0.1', 'localhost,myhost.com,127.0.0.1',
                  'localhost, 2.2.2.2', '*myhost.com', '*']
    hosts = [None, 'example.com', 'http://example.com', 'https://example.com',
            'myhost.com', '2.2.2.2', 'http://myhost.com', 'https://myhost.com',
            'http://abc.myhost.com']

    return itertools.product(http_proxies, https_proxies, no_proxies, hosts)


def test_expect():
    with pytest.raises(ExceptionWithCode) as excinfo:
        expect(False, 1234, 'aaa')
    assert excinfo.value.error_code() == 1234
    assert excinfo.value.description() == 'aaa'

    expect(True, 1234, 'aaa')

@pytest.mark.parametrize('params', list(make_params()))
def test_choose_proxy(params, monkeypatch):
    http_proxy, https_proxy, no_proxy, host = params

    if http_proxy:
        monkeypatch.setenv('http_proxy', http_proxy)
    if https_proxy:
        monkeypatch.setenv('https_proxy', https_proxy)
    if no_proxy:
        monkeypatch.setenv('no_proxy', no_proxy)

    env = Environment()
    proxy = choose_proxy(env, host)

    # return nothing when there is nothing
    if not http_proxy and not https_proxy:
        assert not proxy
        return

    # return nothing if host is in no_proxy list
    if host and no_proxy:
        temp_host = host
        if temp_host.startswith('http'):
            temp_host = temp_host[temp_host.find('://') + 3:]
        if any(fnmatch.fnmatch(temp_host, np.strip()) for np in env.no_proxy):
            assert not proxy
            return

    # return nothing if host is in no_proxy list
    if host and no_proxy and host.startswith('http') \
       and host[host.find('://')+3:] in no_proxy:
        assert not proxy
        return

    # at this point always require proxy
    assert proxy

    # use https_proxy for https
    if https_proxy and host and host.startswith('https://'):
        assert proxy == env.https_proxy
        return

    # otherwise prefer http_proxy and use https_proxy as backup
    if http_proxy:
        assert proxy == env.http_proxy
    else:
        assert proxy == env.https_proxy

def test_find():
    good_list1 = ['foobar', 'abcde', 'qwerty', 'yolo', 'abcd']
    good_list2 = ['ccda', 'foobar', 'abcde', 'qwerty', 'yolo', 'abcd']
    good_list3 = ['foobar', 'qwerty', 'yolo', 'abcd']
    bad_list = ['foobar', 'qwerty', 'yolo']
    empty_list = []
    pred = lambda elem: 'cd' in elem

    assert find(pred, good_list1) == good_list1[1]
    assert find(pred, good_list2) == good_list2[0]
    assert find(pred, good_list3) == good_list3[-1]

    with pytest.raises(LookupError):
        find(pred, bad_list)

    with pytest.raises(LookupError):
        find(pred, empty_list)
