#-*- coding: utf-8 -*- #
# Copyright (C) 2015 Michał Góral.
#
# This file is part of quicksend
#
# quicksend is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# quicksend is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with quicksend. If not, see <http://www.gnu.org/licenses/>.

import collections
import urllib.parse
import pytest

from qs.environ import Environment, Proxy

def _proxy_str(inpr):
    if inpr.proxy_type == 'no_proxy':
        return inpr.host if inpr.host else ''
    protocol = inpr.proxy_type.partition('_')[0]
    ret = '%s://' % protocol
    if inpr.user:
        if inpr.passw:
            ret += '%s:%s@' % (inpr.user, inpr.passw)
        else:
            ret += '%s@' % inpr.user
    if inpr.host:
        ret += inpr.host
    if inpr.port:
        ret += ':%d' % inpr.port
    return ret


def _expected_proxy(inpr):
    if inpr.proxy_type == 'no_proxy':
        if not inpr.host:
            return []
        return [val.strip() for val in inpr.host.split(',')]

    return Proxy(host=inpr.host,
                 port=inpr.port if inpr.port else 1080,
                 username=inpr.user,
                 password=inpr.passw)


def _gen_proxies():
    InputProxy = collections.namedtuple(
        "InputProxy", ["proxy_type", "host", "port", "user", "passw"])

    ret = []
    def _add(*args):
        ret.append(list(args))

    _add(InputProxy('http_proxy', '10.10.1.10', 8080, None, None))
    _add(InputProxy('https_proxy', '10.10.1.10', 8080, None, None))
    _add(InputProxy('no_proxy', 'localhost', None, None, None))
    _add(InputProxy('http_proxy', '10.10.1.10', 8181, 'user', None),
         InputProxy('https_proxy', '10.11.2.13', 8282, 'other', None))
    _add(InputProxy('http_proxy', '10.10.1.10', 8181, 'user', 'pass'),
         InputProxy('https_proxy', '10.11.2.13', 8282, 'other', 'toppass'))
    _add(InputProxy('http_proxy', '10.10.1.10', 8181, 'user', 'pass'),
         InputProxy('https_proxy', '10.11.2.13', 8282, 'other', 'toppass'),
         InputProxy('no_proxy', 'localhost', None, None, None))
    _add(InputProxy('http_proxy', None, None, None, None))
    _add(InputProxy('https_proxy', None, None, None, None))
    _add(InputProxy('no_proxy', None, None, None, None))
    _add(InputProxy('http_proxy', '10.10.1.10', None, None, None))

    return ret


@pytest.mark.parametrize("proxies", _gen_proxies())
def test_proxies(proxies, monkeypatch):
    for proxy in proxies:
        monkeypatch.setenv(proxy.proxy_type, _proxy_str(proxy))

    empty_proxies = {'http_proxy' : None, 'https_proxy' : None, 'no_proxy' : []}
    non_existant_proxies = list(empty_proxies.keys())

    env = Environment()

    for proxy in proxies:
        non_existant_proxies.remove(proxy.proxy_type)
        env_proxy = getattr(env, proxy.proxy_type, None)
        assert env_proxy is not None
        assert env_proxy == _expected_proxy(proxy)

    for proxy_type in non_existant_proxies:
        assert getattr(env, proxy_type) == empty_proxies[proxy_type]

def test_no_proxy_glob(monkeypatch):
    proxies = ['localhost', '127.0.0.1', '127.*', 'abc.com', 'abc.*.com',
               '*abc.com', 'abc?.com', '.abc.com']
    monkeypatch.setenv('no_proxy', ','.join(proxies))

    env = Environment()
    # mgoral: I'm not sure whether globbing expressions containing '*' inside
    # should have another '*' prepended. Let's leave it like that for now, but
    # it may change in future.
    assert len(env.no_proxy) == len(proxies)
    assert env.no_proxy[0] == proxies[0]
    assert env.no_proxy[1] == '*127.0.0.1'
    assert env.no_proxy[2] == '*127.*'
    assert env.no_proxy[3] == '*abc.com'
    assert env.no_proxy[4] == '*abc.*.com'
    assert env.no_proxy[5] == proxies[5]
    assert env.no_proxy[6] == '*abc?.com'
    assert env.no_proxy[7] == '*.abc.com'
